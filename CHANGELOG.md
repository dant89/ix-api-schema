
# Changelog

## 2.5.0 (2023-06-13)

 * `PUT` operations will be deprecated in future releases. (Issue #198)
 * `account_read` was added and will return the authenticated account. (Issue #201)
 * `AvailabilityZones` were added (Issue #156)
 * `mac_acl_protected` property was added to `mp2mp_vc` network service. (Issue #169)
   This also applies to schema versions `2.1.2`, `2.2.2`, `2.3.1` and `2.4.3`.
 * Added IP fields on virtual circuit network-services-configs. (Issue #196)
   This also applies to schema versions `2.1.2`, `2.2.2`, `2.3.1` and `2.4.3`.
 * Polymorphism for codegeneration was improved. (Issue #160)

## 2.4.2 (2022-12-02)
 
 * Improved documentation for `cloud_key` in network services. (Issue #181)
 * Fixed return type of operation: `facilities_read` (Issue #191)
 * Removed unused request parameter `assigned_at` from `macs_list` (Issue #188)
 * Document macs not_valid_before and not_valid_after properties (Issue #189)
 * Operation `roles_read` does no longer accept query parameters (Issue #186)
 * Added service provider pop name to CloudVC product-offering (Issue #187)

## 2.4.1 (2022-10-18)
 
 * Fixes ProductOffering required fields in
   `product_offerings_list` operation.

## 2.4.0 (2022-08-31)
 
 * Added statistics endpoints for ports, connections and
   network services.
 * Introduction of a changelog, to help tracking changes
   between versions.

