
"""
Service Access
--------------

Serialize access objects like connections or port demarcs,
network service access objects.
"""

import enum

from ixapi_schema.coupling import qs
from ixapi_schema.openapi import components
from ixapi_schema.v1.entities import crm, events
from ixapi_schema.v1.constants.access import (
    ConnectionMode,
    LACPTimeout,
    RouteServerSessionMode,
    ConnectionMode,
    BGPSessionType,
    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP,
    NETWORK_SERVICE_CONFIG_TYPE_ELINE,
    NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
    NETWORK_SERVICE_CONFIG_ENTITIES,
    NETWORK_SERVICE_CONFIG_TYPES,
    NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
    NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER,
    NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING,
    NETWORK_FEATURE_CONFIG_ENTITIES,
    NETWORK_FEATURE_CONFIG_TYPES,
)

#
# Connections
#
class ConnectionBase(crm.Ownable, crm.Invoiceable, components.Component):
    """Connection Base"""
    id = components.PrimaryKeyRelatedField(read_only=True)
    mode = components.EnumField(
        ConnectionMode,
        help_text="""
            Sets the mode of the connection. The mode can be:

            - `lag_lacp`: connection is build as a LAG with LACP enabled
            - `lag_static`: connection is build as LAG with static configuration
            - `flex_ethernet`: connect is build as a FlexEthernet channel
            - `standalone`: only one demarc is allowed in this connection without
            any bundling.

            Example: "lag_lacp"
        """)
    lacp_timeout = components.EnumField(
        LACPTimeout,
        required=False,
        default=None,
        allow_null=True,
        help_text="""
            This sets the LACP Timeout mode. Both ends of the connections need
            to be configured the same.

            Example: "slow"
        """)

    speed = components.IntegerField(
        allow_null=True,
        required=False,
        min_value=0,
        help_text="""
            Shows the total bandwidth of the connection in Mbit/s.

            Example: 20000
        """)


class Connection(events.Stateful, ConnectionBase):
    """Connection"""
    name = components.CharField(max_length=80,
                                 required=False,
                                 allow_blank=True)

    demarcs = components.PrimaryKeyRelatedField(
        source="demarcation_points",
        many=True,
        required=False,
        queryset=qs.Any("access.DemarcationPoint"),
        help_text="""
            Reference to the demarcs belonging to this connection. Typically
            all demarcs within one connection are distributed over the same
            device.

            Example: ["ID23", "42", "5"]
        """)


class ConnectionRequest(ConnectionBase):
    """Connection Request"""


class ConnectionUpdate(ConnectionBase):
    """Connection Update"""

#
# Demarcation Points
#
class DemarcationPointBase(
        crm.Ownable,
        crm.Invoiceable,
        components.Component,
    ):
    """Demarcation Point Base"""
    id = components.PrimaryKeyRelatedField(read_only=True)

    connection = components.PrimaryKeyRelatedField(
        allow_null=True,
        required=False,
        queryset=qs.Any("access.Connection"))

    speed = components.IntegerField(
        read_only=True,
        min_value=0,
        default=None,
        allow_null=True)


class DemarcationPoint(events.Stateful, DemarcationPointBase):
    """Demarc"""
    name = components.CharField(
        max_length=80,
        read_only=True,
        default="",
        help_text="""
            Name of the demarcation point (set by the IXP)
        """)
    media_type = components.CharField(
        max_length=20,
        help_text="""
            The media type of the interface.
            See the device's capabilities to see what types
            are available.

            Example: "10GBASE-LR"
        """)

    # Relations Relation
    pop = components.PrimaryKeyRelatedField(
        source="point_of_presence",
        queryset=qs.Any("catalog.PointOfPresence"))


class DemarcationPointRequest(DemarcationPointBase):
    """Demarc Request"""
    # Relations Relation
    pop = components.PrimaryKeyRelatedField(
        source="point_of_presence",
        queryset=qs.Any("catalog.PointOfPresence"))

    media_type = components.CharField(
        max_length=20,
        help_text="""
            The media type of the interface.
            See the device's capabilities to see what types
            are available.

            Example: "10GBASE-LR"
        """)

class DemarcationPointUpdate(DemarcationPointBase):
    """Demarc Update"""


#
# Network Service Configs
#
class NetworkServiceConfigBase(
        crm.Ownable,
        crm.Invoiceable,
        components.Component,
    ):
    """Network Service Config"""
    id = components.PrimaryKeyRelatedField(read_only=True)

    inner_vlan = components.IntegerField(
        allow_null=True,
        required=False,
        default=None,
        min_value=0,
        max_value=4096,
        help_text="""
            This is a VLAN ID.
        """)

    capacity = components.IntegerField(
        required=False,
        default=None,
        min_value=1,
        allow_null=True,
        help_text="""
            The capacity of the service in Mbps. If set to Null,
            the maximum capacity will be used.
            That means, the service can consume up
            to the total bandwidth of the `connection`.

            Typically the service is charged based on the capacity.
        """)

    connection = components.PrimaryKeyRelatedField(
        queryset=qs.Any("access.Connection"),
        help_text="""
            The id of the connection to use for this service config. This
            associates the service to a LAG. All traffic comming in on the
            connection with the correct VLAN-ID will be transported to this
            service.
        """)

    network_feature_configs = components.PrimaryKeyRelatedField(
        many=True,
        read_only=True,
        help_text="""
            A list of ids of network feature configurations.

            Example: ["12356", "43829"]
        """)


class NetworkServiceConfigRequestBase(crm.Ownable, components.Component):
    """Network Service Config Request"""
    network_service = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkService"),
        help_text="""
            The id of the network service to configure.
        """)

    # When requesting a new network service config,
    # we accept a set of vlan ranges.
    outer_vlan = components.ListField(
        required=True,
        child=components.ListField(
            child=components.IntegerField(
                min_value=0,
                max_value=4096),
            min_length=1,
            max_length=2),
        min_length=1,
        help_text="""
            Request a vlan from a set of ranges.
            A range is represented as a list of integers.
            The range has a max length of 2.

            A set of ranges is a list of lists.

            In case only one bound in the range is present,
            it is interpreted as the exact value. That means that `[23]` is
            equivalent to `[23, 23]`.

            To just let the platform choose for you, provide all
            possible ranges. `[ [0, 4096] ]`.

            Example: [[0, 4096]]
        """)


class NetworkServiceConfigUpdateBase(crm.Ownable, components.Component):
    """Network Service Config Update"""
    # See network service config input
    outer_vlan = components.ListField(
        child=components.ListField(
            child=components.IntegerField(
                min_value=0,
                max_value=4096),
            default=[],
            min_length=1,
            max_length=2),
        default=[],
        min_length=1,
        help_text="""
            Request a vlan from a set of ranges.
            A range is represented as a list of integers.
            The range has a max length of 2.

            A set of ranges is a list of lists.

            In case only one bound in the range is provided,
            it is interpreted as the exact value. `[23]` is
            equivalent to `[23, 23]`.

            To just let the platform choose for you, provide all
            possible ranges. `[ [0, 4096] ]`.

            Example: [[0, 4096]]
        """)


class NetworkServiceConfigOutputBase(
        events.Stateful,
        components.Component,
    ):
    """Network Service Config"""
    network_service = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.Networkservice"),
        help_text="""
            The id of the configured network service.
        """)

    outer_vlan = components.IntegerField(
        min_value=0,
        max_value=4096,
        help_text="""
            A vlan id, where `0` is untagged.
            Any value > 0 will be the tagged vlan.

            Example: 300
        """)

# Exchange Lan

class ExchangeLanNetworkServiceConfigBase(NetworkServiceConfigBase):
    """Exchange Lan Network Service Config"""
    asns = components.ListField(
        child=components.IntegerField(min_value=0, max_value=4294967295),
        default=[],
        min_length=0,
        max_length=20)

    macs = components.PrimaryKeyRelatedField(
        source="mac_addresses",
        required=False,
        queryset=qs.Any("ipam.MacAddress"),
        many=True)

    ips = components.PrimaryKeyRelatedField(
        source="ip_addresses",
        many=True,
        read_only=True)

    # Mark as polymorphic to inject a 'type'
    __polymorphic_type__ = \
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN


class ExchangeLanNetworkServiceConfigRequest(
        NetworkServiceConfigRequestBase,
        ExchangeLanNetworkServiceConfigBase,
    ):
    """Exchange Lan Network Service Config Request"""


class ExchangeLanNetworkServiceConfigUpdate(
        NetworkServiceConfigUpdateBase,
        ExchangeLanNetworkServiceConfigBase,
    ):
    """Exchange Lan Network Service Config Update"""


class ExchangeLanNetworkServiceConfig(
        NetworkServiceConfigOutputBase,
        ExchangeLanNetworkServiceConfigBase,
    ):
    """Exchange Lan Network Service Config"""

# Closed User Group

class ClosedUserGroupNetworkServiceConfigBase(
        NetworkServiceConfigBase,
    ):
    """Closed User Group Network Service Config"""
    macs = components.PrimaryKeyRelatedField(
        source="mac_addresses",
        many=True,
        read_only=True)

    __polymorphic_type__ = \
        NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP
    __hidden_in_docs__ = True


class ClosedUserGroupNetworkServiceConfigRequest(
        NetworkServiceConfigRequestBase,
        ClosedUserGroupNetworkServiceConfigBase,
    ):
    """Closed User Group Network Service Config Request"""


class ClosedUserGroupNetworkServiceConfigUpdate(
        NetworkServiceConfigUpdateBase,
        ClosedUserGroupNetworkServiceConfigBase,
    ):
    """Closed User Group Network Service Update"""


class ClosedUserGroupNetworkServiceConfig(
        NetworkServiceConfigOutputBase,
        ClosedUserGroupNetworkServiceConfigBase,
    ):
    """Closed User Group Network Service Config"""

# E-Line

class ELineNetworkServiceConfigBase(
        NetworkServiceConfigBase,
    ):
    """ELine Network Service Config"""
    __polymorphic_type__ = \
        NETWORK_SERVICE_CONFIG_TYPE_ELINE
    __hidden_in_docs__ = True


class ELineNetworkServiceConfigRequest(
        NetworkServiceConfigRequestBase,
        ELineNetworkServiceConfigBase,
    ):
    """ELine Network Service Config Request"""

class ELineNetworkServiceConfigUpdate(
        NetworkServiceConfigUpdateBase,
        ELineNetworkServiceConfigBase,
    ):
    """ELine Network Service Config Update"""

class ELineNetworkServiceConfig(
        NetworkServiceConfigOutputBase,
        ELineNetworkServiceConfigBase,
    ):
    """ELine Network Service Config"""

# Cloud

class CloudNetworkServiceConfigBase(
        NetworkServiceConfigBase,
    ):
    """Cloud Network Service Config"""
    cloud_key = components.CharField(max_length=512)

    __hidden_in_docs__ = True
    __polymorphic_type__ = \
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD


class CloudNetworkServiceConfigRequest(
        NetworkServiceConfigRequestBase,
        CloudNetworkServiceConfigBase,
    ):
    """Cloud Network Service Config Request"""


class CloudNetworkServiceConfigUpdate(
        NetworkServiceConfigUpdateBase,
        CloudNetworkServiceConfigBase,
    ):
    """Cloud Network Service Config Update"""


class CloudNetworkServiceConfig(
        NetworkServiceConfigOutputBase,
        CloudNetworkServiceConfigBase,
    ):
    """Cloud Network Service Config"""


class NetworkServiceConfigRequest(components.PolymorphicComponent):
    """Polymorhic Network Service Config Request"""
    serializer_classes = {
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkServiceConfigRequest,
        NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP:
            ClosedUserGroupNetworkServiceConfigRequest,
        NETWORK_SERVICE_CONFIG_TYPE_ELINE:
            ELineNetworkServiceConfigRequest,
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
            CloudNetworkServiceConfigRequest,
    }

    entity_types = NETWORK_SERVICE_CONFIG_ENTITIES


class NetworkServiceConfigUpdate(components.PolymorphicComponent):
    """Polymorphic Network Service Config"""
    serializer_classes = {
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkServiceConfigUpdate,
        NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP:
            ClosedUserGroupNetworkServiceConfigUpdate,
        NETWORK_SERVICE_CONFIG_TYPE_ELINE:
            ELineNetworkServiceConfigUpdate,
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
            CloudNetworkServiceConfigUpdate,
    }

    entity_types = NETWORK_SERVICE_CONFIG_ENTITIES


class NetworkServiceConfig(components.PolymorphicComponent):
    """Polymorphic Network Service Config"""
    serializer_classes = {
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkServiceConfig,
        NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP:
            ClosedUserGroupNetworkServiceConfig,
        NETWORK_SERVICE_CONFIG_TYPE_ELINE:
            ELineNetworkServiceConfig,
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
            CloudNetworkServiceConfig,
    }

    entity_types = NETWORK_SERVICE_CONFIG_ENTITIES

#
# Feature Configurations
#
class NetworkFeatureConfigBase(
        crm.Ownable,
        crm.Invoiceable,
        components.Component,
    ):
    """A feature config base serializer"""
    id = components.PrimaryKeyRelatedField(read_only=True)

    network_feature = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkFeature"))
    network_service_config = components.PrimaryKeyRelatedField(
        queryset=qs.Any("access.NetworkServiceConfig"))


class NetworkFeatureConfigUpdateBase(crm.Ownable, components.Component):
    """Network Feature Configu Updates Base"""


class RouteServerNetworkFeatureConfigBase(components.Component):
    """Routeserver Config Feature"""
    asn = components.IntegerField(
        min_value=0,
        max_value=4294967295,
        help_text="""
            The ASN of the peer.

            Example: 4200000023
        """)
    password = components.CharField(
        required=False,
        default="",
        max_length=128,
        allow_blank=True,
        allow_null=False,
        help_text="""
            The cleartext BGP session password
            Example: bgp-session-test-23
        """)

    as_set_v4 = components.CharField(
        max_length=100,
        required=False,
        allow_null=True,
        help_text="""
            AS-Set of the customer for IPv4 prefix filtering.
            This is used to generate
            filters on the router servers.
            Only valid referenced prefixes within the AS-Set
            are allowed inbound to the route server. All other routes are
            filtered.

            This field is *required* with session mode `public`
            if the route server network feature supports the `af_inet`
            address family.

            Important: The format has to be: "AS-SET@IRR". IRR is the database
            where the AS-SET is registred. Typically used IRR's are RADB, RIPE,
            NTTCOM, APNIC, ALTDB, LEVEL3, ARIN, AFRINIC, LACNIC

            Example: MOON-AS@RIPE
        """)
    as_set_v6 = components.CharField(
        max_length=100,
        required=False,
        allow_null=True,
        help_text="""
            AS-Set of the customer for IPv6. This is used to generate filters
            on the router servers. Only valid referenced prefixes within
            the AS-Set are allowed inbound to the route server.
            All other routes are filtered.

            This field is *required* with session mode `public`
            if the route server network feature supports the `af_inet6`
            address family.

            Important: The format has to be: "AS-SET@IRR". IRR is the database
            where the AS-SET is registred. Typically used IRR's are RADB, RIPE,
            NTTCOM, APNIC, ALTDB, LEVEL3, ARIN, AFRINIC, LACNIC

            Example: MOON-AS@RIPE
        """)
    max_prefix_v4 = components.IntegerField(
        required=False,
        allow_null=True,
        min_value=0,
        help_text="""
            Announcing more than `max_prefix` IPv4 prefixes the bgp
            session will be droped.

            Example: 5000
        """)
    max_prefix_v6 = components.IntegerField(
        required=False,
        allow_null=True,
        min_value=0,
        help_text="""
            Announcing more than `max_prefix` IPv6 prefixes the bgp
            session will be droped.

            Example: 5000
        """)
    insert_ixp_asn = components.BooleanField(
        default=True,
        required=False,
        help_text="""
            Insert the ASN of the IXP into the AS path. This function is only
            used in special cases. In 99% of all cases, it should be false.

            Example: false
        """)

    session_mode = components.EnumField(
        RouteServerSessionMode,
        help_text="""
            Set the session mode with the routeserver.

            Example: public
        """)
    bgp_session_type = components.EnumField(
        BGPSessionType,
        help_text="""
            The session type describes which of the both parties will open the
            connection. If set to passive, the customer router needs to open
            the connection. If its set to active, the route server will open
            the connection. The standard behavior on most IX's is passive.

            Example: passive
        """)

    __polymorphic_type__ = \
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER


class RouteServerNetworkFeatureConfigUpdate(
        NetworkFeatureConfigUpdateBase,
        RouteServerNetworkFeatureConfigBase,
    ):
    """Route Server Network Feature Config Update"""


class RouteServerNetworkFeatureConfigRequest(
        NetworkFeatureConfigBase,
        RouteServerNetworkFeatureConfigBase,
    ):
    """Route Server Network Feature Config Request"""


class RouteServerNetworkFeatureConfig(
        events.Stateful,
        NetworkFeatureConfigBase,
        RouteServerNetworkFeatureConfigBase,
    ):
    """Route Server Network Feature Config"""


class IXPRouterNetworkFeatureConfigBase(components.Component):
    """IXP Router Feature Config"""
    max_prefix = components.IntegerField(min_value=0)
    password = components.CharField(
        required=False,
        default=None,
        max_length=128,
        allow_blank=True,
        allow_null=True)
    bgp_session_type = components.EnumField(BGPSessionType)

    __hidden_in_docs__ = True
    __polymorphic_type__ = \
        NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER


class IXPRouterNetworkFeatureConfigUpdate(
        NetworkFeatureConfigUpdateBase,
        IXPRouterNetworkFeatureConfigBase,
    ):
    """IXP Router Network Feature Config Update"""


class IXPRouterNetworkFeatureConfigRequest(
        NetworkFeatureConfigBase,
        IXPRouterNetworkFeatureConfigBase,
    ):
    """IXP Router Network Feature Config Request"""


class IXPRouterNetworkFeatureConfig(
        events.Stateful,
        NetworkFeatureConfigBase,
        IXPRouterNetworkFeatureConfigBase,
    ):
    """IXP Router Network Feature Config"""


class BlackholingNetworkFeatureConfigBase(components.Component):
    """Blackholing Configuration Base"""
    activated = components.BooleanField()
    ixp_specific_configuration = components.CharField(
        max_length=2048,
        default=None,
        allow_blank=True,
        allow_null=True)

    filtered_prefixes = components.PrimaryKeyRelatedField(
        queryset=qs.Any("ipam.IpAddress"),
        many=True)

    __hidden_in_docs__ = True
    __polymorphic_type__ = \
        NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING


class BlackholingNetworkFeatureConfigUpdate(
       NetworkFeatureConfigUpdateBase,
       BlackholingNetworkFeatureConfigBase,
    ):
    """Blackholing Network Feature Config Update"""


class BlackholingNetworkFeatureConfigRequest(
        NetworkFeatureConfigBase,
        BlackholingNetworkFeatureConfigBase,
    ):
    """Blackholing Network Feature Config Request"""


class BlackholingNetworkFeatureConfig(
        events.Stateful,
        NetworkFeatureConfigBase,
        BlackholingNetworkFeatureConfigBase,
    ):
    """Blackholing Network Feature Config"""


class NetworkFeatureConfig(components.PolymorphicComponent):
    """Polymorphic Network Feature Config"""
    serializer_classes = {
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
            RouteServerNetworkFeatureConfig,
        NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER:
            IXPRouterNetworkFeatureConfig,
        NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING:
            BlackholingNetworkFeatureConfig,
    }

    entity_types = NETWORK_FEATURE_CONFIG_ENTITIES


class NetworkFeatureConfigRequest(components.PolymorphicComponent):
    """Polymorphic Network Feature Config Request"""
    serializer_classes = {
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
            RouteServerNetworkFeatureConfigRequest,
        NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER:
            IXPRouterNetworkFeatureConfigRequest,
        NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING:
            BlackholingNetworkFeatureConfigRequest,
    }

    entity_types = NETWORK_FEATURE_CONFIG_ENTITIES


class NetworkFeatureConfigUpdate(components.PolymorphicComponent):
    """Polymorphic Network Feauture Config Update"""
    serializer_classes = {
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
            RouteServerNetworkFeatureConfigUpdate,
        NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER:
            IXPRouterNetworkFeatureConfigUpdate,
        NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING:
            BlackholingNetworkFeatureConfigUpdate,
    }

    entity_types = NETWORK_FEATURE_CONFIG_ENTITIES


