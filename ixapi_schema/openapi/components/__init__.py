
import enum

from django.conf import settings
from django.core import exceptions

# Export rest framework components
from rest_framework.serializers import *

# Test if django was set up correctly
DJANGO_CONTEXT = True
try:
    settings.VAR
except exceptions.ImproperlyConfigured:
    DJANGO_CONTEXT = False
except AttributeError:
    pass # We are testing for a non existing variable


if DJANGO_CONTEXT:
    from ixapi_schema.openapi.components.django import *
else:
    from ixapi_schema.openapi.components.shim import *


from ixapi_schema.openapi.components.filters import *


# Custom constants

class IpVersion(enum.Enum):
    IPV4 = 4
    IPV6 = 6


class AddressFamilies(enum.Enum):
    AF_INET = "af_inet"
    AF_INET6 = "af_inet6"


# CustomFields

class JWTField(CharField):
    pass


class RefreshTokenField(JWTField):
    pass


class IpVersionField(IntegerField):
    def to_representation(self, value):
        """
        Serialize ip version.

        :param value: An IpVersion enum
        """
        return value.value


    def to_internal_value(self, data):
        """
        Deserialize ip version

        :param data: IP version integer
        """
        return IpVersion(data)


class MacAddressField(CharField):
    """
    Derived from a char field, we validate that the input
    is a mac address.
    """
    def to_internal_value(self, data):
        """Parse and validate mac address"""
        if not isinstance(data, str):
            raise ValidationError(
                "A MAC address input must be a string")

        if not (len(data) > 10 and len(data) <= 17):
            raise ValidationError(
                "Invalid MAC address length")

        tokens = data.split(":")
        if not len(tokens) == 6:
            raise ValidationError(
                "MAC address too short")

        try:
            [int(t, 16) for t in tokens]
        except ValueError:
            raise ValidationError(
                "A mac address must only contain hexadecimal byte values")

        # Normalize
        return data.lower()


class TimeseriesSampleField(ListField):
    """Timeseries sample field"""
    child=IntegerField()
    min_items=5
    max_items=5
