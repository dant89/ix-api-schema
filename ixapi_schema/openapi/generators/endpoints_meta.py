
"""
From endpoints metadata generate paths specs.
"""

import re
import collections

from rest_framework import serializers

from ixapi_schema.openapi.generators import components_meta, filters_meta
from ixapi_schema.openapi.utils import text as text_utils
from ixapi_schema.v1.entities import problems


RE_PATH_PARAMS = re.compile(r"(<(\w+):(\w+)>)")


def format_path(path):
    """
    Format path to make it openapi compliant.
    """
    params = RE_PATH_PARAMS.findall(path)
    for match, name, _ in params:
        path = path.replace(match, '{' + name + '}')
    return path


def encode_path_params(path):
    """
    Extract path parameters, where a param has the
    format <name:type>.

    :param path: A path like /some/endpoint/<pk:int>
    :returns: A list of path parameters.
    """
    params = RE_PATH_PARAMS.findall(path)
    return [_path_param_frag(param)
            for param in params]


def _path_param_frag(param):
    """
    Generate path parameter schema

    :param param: The parameter to encdoe. This is
        a tuple with the name and paramerter type.
    """
    _, name, ptype = param
    return {
        "name": name,
        "in": "path",
        "required": True,
        "description": f"Get by {name}",
        "schema": {
            "type": encode_path_type(ptype),
            "title": "",
            "description": "",
        },
    }


def encode_path_type(ptype):
    """Encode path type"""
    # For now we only support str(ings) and int(egers).
    if ptype in ["int", "integer"]:
        return "integer"
    if ptype in ["str", "string"]:
        return "string"

    # In case we run into an undefined type
    raise ValueError(ptype)


#
# Endpoints: Paths
#
def get_operation(endpoint, name):
    """
    Get a single operation from endpoint by name.

    :param name: The operation's name
    """
    name = name.lower()
    for k, v in endpoint.items():
        if k.lower() == name:
            return v
    return None


def get_operations(endpoint):
    """
    Get operations from endpoint.
    """
    operations = {}
    op = get_operation(endpoint, "get")
    if op:
        operations["get"] = op
    op = get_operation(endpoint, "post")
    if op:
        operations["post"] = op
    op = get_operation(endpoint, "put")
    if op:
        operations["put"] = op
    op = get_operation(endpoint, "patch")
    if op:
        operations["patch"] = op
    op = get_operation(endpoint, "delete")
    if op:
        operations["delete"] = op
    op = get_operation(endpoint, "options")
    if op:
        operations["options"] = op

    return operations


def get_operation_id(info, path, verb):
    """
    The operationId is build by convention:
        <endpoint_name>.
    """
    tokens = path.split("/")
    endpoint_name = info.get("endpoint", info["tag"])

    if verb == "get":
        if tokens[-1] == endpoint_name:
            # This is by convention the list operation.
            op = "list"
        else:
            op = "read"
    elif verb == "post":
        op = "create"
    elif verb == "put":
        op = "update"
    elif verb == "patch":
        op = "partial_update"
    elif verb == "delete":
        op = "destroy"
    elif verb == "options":
        if tokens[-1] == endpoint_name:
            # This is by convention the list operation.
            op = "collection_meta"
        else:
            op = "meta"

    # Canonicalize
    op_id = f"{endpoint_name}_{op}"
    op_id = op_id.replace("-", "_")

    return op_id



def encode_component(component, content_type="application/json"):
    """Encode component in response."""
    if component == "LOA":
        return encode_loa_response()
    if component == "UPLOAD_SUCCESS":
        return encode_upload_success()

    if isinstance(component, (
            serializers.ListSerializer,
            serializers.ListField)):
        return encode_component_list(component, content_type)

    return encode_component_ref(component, content_type)


def encode_loa_response():
    """The response will contain a LOA"""
    frag = {
        "description": """A Letter Of Authorization""",
        "content": {
            "application/pdf": {
                "schema": {
                    "type": "string",
                    "format": "binary",
                },
            },
            "application/octet-stream": {
                "schema": {
                    "type": "string",
                    "format": "binary",
                },
            },
            "text/plain": {
                "schema": {
                    "type": "string",
                },
            },
        },
    }
    return frag


def encode_upload_success():
    """Upload was successful"""
    frag = {
        "description": """The upload was successful""",
    }
    return frag


def encode_component_ref(component, content_type):
    """Encode a reference component"""
    ref = components_meta.component_ref(component)
    description = text_utils.trim_docstring(component.__doc__)
    frag = {
        "description": description,
        "content": {
            content_type: {
                "schema": {
                    "$ref": ref,
                },
            },
        },
    }
    return frag


def encode_component_list(component, content_type):
    """Encode a reference component"""
    ref = components_meta.component_ref(component.child)
    description = text_utils.trim_docstring(component.child.__doc__)
    frag = {
        "description": "List of: " + description,
        "content": {
            content_type: {
                "schema": {
                    "type": "array",
                    "items": {
                        "$ref": ref,
                    },
                },
            },
        },
    }
    return frag


def encode_extra_field_components(extra_components):
    """
    Encode schema for extra fields of a problem
    """
    props = {name: components_meta.spec_from_field(field)
             for extra in extra_components
             for name, field in extra.items()}
    if not props:
        return {}
    return {
        "type": "object",
        "properties": props,
    }


def encode_problem_response_group(error_base, problem_group):
    """Encode a problem"""
    # Use the problems response serializer
    ref = components_meta.component_ref(problems.ProblemResponse())

    problem_types = [error_base+prob.ptype+".html" for prob in problem_group]
    status = [prob.response_status for prob in problem_group][0]
    description = [prob.__class__.__name__ for prob in problem_group][0]
    extra_fields = encode_extra_field_components(
        getattr(prob, "extra_fields", {})
        for prob in problem_group)

    description = description.replace("Problem", "")
    frag = {
        "description": description, # problem_group[0].ptype,
        "content": {
            "application/json": {
                "schema": {
                    "allOf": [
                        {"$ref": ref},
                        {
                            "type": "object",
                            "properties": {
                                "type": {
                                    "type": "string",
                                    "enum": problem_types,
                                },
                                "title": {
                                    "example":
                                        problem_group[0].title
                                },
                                "status": {
                                    "example": status,
                                },
                            },
                        },
                        extra_fields,
                    ],
                },
            },
        },
    }
    return frag


def encode_operation_responses(api, operation):
    """
    Encode responses from operation:
    These are the operations responses together
    with the problem responses.
    """
    # Encode entity responses
    responses = {
        status: encode_component(component)
        for status, component in operation["responses"].items()
    }

    # Group problems by status code
    problem_groups = collections.defaultdict(list)
    for problem in operation.get("problems", []):
        problem_groups[problem.response_status].append(problem)

    # Encode problems
    problem_responses = {
        status: encode_problem_response_group(
            api["error_documents_base_url"], group)
        for status, group in problem_groups.items()
    }

    responses.update(problem_responses)

    return responses


def encode_filter_params(operation):
    """Encode all query params"""
    return [filters_meta.frag_from_filter(name, filter_field)
            for name, filter_field in operation.get("filters", {}).items()]


def encode_operation(api, info, operation, path, verb):
    """
    Encode a operation into an openapi3 schema.
    """
    op_id = get_operation_id(info, path, verb)
    if operation.get("operation"): # Override
        op_id = operation["operation"]

    description = text_utils.trim_docstring(
        operation["description"])

    responses = encode_operation_responses(api, operation)

    # Base schema
    schema = {
        "operationId": op_id,
        "description": description,
        "tags": [info["tag"]],
        "responses": responses,
    }

    # Add security information if present
    security = operation.get("security")
    if security is not None:
        schema["security"] = security

    # Encode *optional* operation request schema
    if operation.get("request"):
        request_content_type = operation.get(
            "request_content_type", "application/json")

        request_body = encode_component(
            operation["request"],
            content_type=request_content_type)
        schema["requestBody"] = request_body

    # Get parameters and add to schema if there are any:
    params = encode_path_params(path) + encode_filter_params(operation)
    if params:
        schema["parameters"] = params

    return schema


def encode_path(api, _endpoint, path, info):
    """
    Get schema from endpoint
    """
    operations = get_operations(info)
    return {verb: encode_operation(api, info, op, path, verb)
            for verb, op in operations.items()}


def encode_paths(api, endpoints):
    """
    Get paths from a list of endpoints
    """
    return {format_path(path): encode_path(api, endpoint, path, info)
            for endpoint in endpoints
            for path, info in endpoint["paths"].items()}

#
# Endpoints: Tags
#
def encode_tags(endpoints):
    """
    Get all tags from the endpoints
    """
    tags = [tag for _, tag in {
        info["tag"]: {
            "name": info["tag"],
            "description": text_utils.trim_docstring(info["description"]),
        } for endpoint in endpoints
        for _, info in endpoint["paths"].items()
        if info.get("description")
    }.items()]
    return sorted(tags, key=lambda t: t["name"])
    # I need to cut back with the erlang.
