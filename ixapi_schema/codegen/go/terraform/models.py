"""
Terraform Models Renderer
"""

from collections.abc import Mapping
from subprocess import run
from os import path

from ixapi_schema.codegen.go import dicts
from ixapi_schema.codegen.go.components import (
    is_writeonly_attr,
    is_readonly_attr,
    is_readonly_model,
    is_override_attr,
)
from ixapi_schema.codegen.go.text import (
    idiomize,
    camelize,
    comment_newlines,
)
from ixapi_schema.v2 import __version__ as schema_version

PRELUDE = """package provider

//
// CAUTION:
//   This file is generated from the IX-API
//   openapi specs. DO NOT EDIT.
//

"""

def models_from_components(components):
    """Prepares models for rendering"""
    models = {}

    excluded = [
        "AuthToken",
    ]

    # Skip all Request and Update types, the will be merged
    for name, comp in components.items():
        if name in excluded:
            continue
        if name.endswith("Request"):
            continue
        if name.endswith("Update"):
            continue
        if name.endswith("Patch"):
            continue
        models[name] = comp

    # Combine resource response model, e.g. Account
    # with update and request model
    for name, model in models.items():
        update = components.get(name + "Patch", {})
        request = components.get(name + "Request", {})
        models[name] = dicts.merge(request, update, model)

        # Use required fields only from request / create
        # as updates are handled using the patch operation.
        models[name]["required"] = request.get("required", [])

    return models


def render_models(spec):
    """Render all models from openapi spec"""
    buf = ""
    components = spec["components"]["schemas"]

    models = models_from_components(components)

    for name, component in models.items():
        buf += render_model(name, component)
    return buf


def render_model(name, component):
    """Render a single model component"""
    type_name = idiomize(name)
    description = comment_newlines(
        component.get("description", f"is {name}"))

    props = component.get("properties")
    if not props:
        print(f"skipping {name}, no properties")
        return ""

    # TODO: Handle polymorphic properties like
    #       vlan_config by separating them into distinct
    #       fields. Eg. `port_vlan_config`, `qinq_vlan_config`
    props = {name: prop for name, prop in props.items()
             if name != "type" and name != "vlan_config"}

    buf = ""

    # Model struct
    buf += "// " + type_name + " " + description + "\n"
    buf += "type " + type_name + " struct {\n"
    for pname, prop in props.items():
        buf += render_model_attr(pname, prop)
    buf += "}\n\n"

    # Model types mapping
    buf += "// " + type_name + "Types is the type mapping for the model\n"
    buf += "var " + type_name + "Types = map[string]attr.Type{\n"
    for pname, prop in props.items():
        buf += render_model_typemap(pname, prop)
    buf += "}\n\n"

    # Model schema
    buf += "// " + type_name + "Schema is the terraform schema for the model\n"
    buf += "var " + type_name + "Schema = map[string]tfsdk.Attribute{\n"
    for pname, prop in props.items():
        buf += render_model_schema_attr(component, pname, prop)
    buf += "}\n\n"

    # Model conversion funcs
    buf += model_func_from_response(name, component)
    buf += model_func_list_from_response(name, component)
    buf += model_func_from_object(name, component)

    return buf


def render_model_schema_attr(component, pname, prop):
    """Render a property in the schema"""
    required = prop_is_required(component, pname)
    readonly = is_readonly_attr(pname)

    buf = ""
    buf += "\"" + pname + "\": {\n"
    # Type / Attributs
    buf += model_schema_attr_prop(prop)

    # Set required / optional
    if required and readonly:
        buf += "Computed: true,\n"
    elif required:
        buf += "Required: true,\n"
    else:
        buf += "Optional: true,\n"
        buf += "Computed: true,\n"

    buf += "},\n"

    return buf


def model_schema_attr_prop(prop):
    """schema property type"""
    ptype = prop.get("type")
    ref_type = ref_type_from_prop(prop)
    buf = ""
    list_ref_type = ref_type_from_prop(prop.get("items", {}))

    if ptype and ptype == "array" and list_ref_type:
        buf += "Attributes: tfsdk.ListNestedAttributes(\n"
        buf += schema_type_from_prop(prop) + ",\n"
        buf += " tfsdk.ListNestedAttributesOptions{}),\n"
    elif ptype and ptype == "array":
        buf += "Type: types.ListType{\n"
        buf += "  ElemType: " + schema_type_from_prop(prop) + ",\n"
        buf += "},\n"
    elif ptype and ptype == "object":
        buf += "Type: types.MapType{\n"
        buf += "  ElemType: types.StringType,\n"
        buf += "},\n"
    elif ref_type:
        buf += "Attributes: tfsdk.SingleNestedAttributes("
        buf += schema_type_from_prop(prop)
        buf += "),\n"
    else:
        buf += "Type: " + schema_type_from_prop(prop) + ",\n"

    return buf


def schema_type_from_prop(prop):
    """Schema property type"""
    ref_type = ref_type_from_prop(prop)
    if ref_type:
        return ref_type + "Schema"

    ptype = prop["type"]
    if ptype == "array":
        return schema_type_from_prop(prop["items"])
    if ptype == "string":
        return "types.StringType"
    if ptype == "integer":
        return "types.Int64Type"
    if ptype == "boolean":
        return "types.BoolType"

    raise NotImplementedError("missing type", ptype, prop)


def model_func_from_response(name, component):
    """Decode response into model"""
    type_name = idiomize(name)
    props = {name: prop for name, prop
             in component["properties"].items()
             if name != "type" and name != "vlan_config"}

    buf = ""
    buf += f"// {type_name}FromResponse converts from ix-api into state.\n"
    buf += "func " + type_name + "FromResponse(\n"
    buf += "  diags *diag.Diagnostics,\n"
    buf += "  res *ixapi." + type_name + ",\n"
    buf += ") types.Object {\n"
    buf += "  obj := types.Object{\n"
    buf += "     Attrs: make(map[string]attr.Value),\n"
    buf += "     AttrTypes: " + type_name + "Types,\n"
    buf += "  }\n"
    buf += "  if res == nil {\n"
    buf += "     obj.Null = true\n"
    buf += "  }\n\n"

    for name, prop in props.items():
        required = prop_is_required(component, name)
        if is_readonly_model(type_name):
            required = True
        if is_writeonly_attr(name):
            continue

        buf += decode_prop_from_response(name, prop, required)

    buf += "\n"
    buf += "  return obj"
    buf += "}\n\n"

    return buf


def model_func_list_from_response(name, component):
    """Decode into list object"""
    type_name = idiomize(name)
    props = {name: prop for name, prop 
             in component["properties"].items()
             if name != "type" and name != "vlan_config"}

    buf = ""
    buf += f"// {type_name}ListFromResponse creates a list of {type_name}.\n"
    buf += "func " + type_name + "ListFromResponse(\n"
    buf += "  diags *diag.Diagnostics,\n"
    buf += "  res []*ixapi." + type_name + ",\n"
    buf += ") types.List {\n"
    buf += "  list := make([]attr.Value, len(res))\n"
    buf += "  for i, item := range res {\n"
    buf += "    list[i] = " + type_name + "FromResponse(diags, item)"
    buf += "  }\n"
    buf += "  return types.List{\n"
    buf += "    Elems: list,\n"
    buf += "    ElemType: types.ObjectType{\n"
    buf += "       AttrTypes: " + type_name + "Types,\n"
    buf += "    },\n"
    buf += "  }\n"
    buf += "}\n\n"

    return buf


def model_func_from_object(name, component):
    """Decode object into IX-API type"""
    type_name = idiomize(name)
    props = {name: prop for name, prop 
             in component["properties"].items()
             if name != "type" and name != "vlan_config"}

    if is_readonly_model(type_name):
        return ""

    buf = ""
    buf += f"// {type_name}FromObject converts state into ix-api.\n"
    buf += "func " + type_name + "FromObject(\n"
    buf += "  ctx context.Context,\n"
    buf += "  diags *diag.Diagnostics,\n"
    buf += "  obj types.Object,\n"
    buf += ") *ixapi." + type_name + " {\n"

    # Empty yields nil
    buf += "  if obj.Null || obj.Unknown {\n return nil\n }\n\n "

    # Decode object
    buf += "  data := " + type_name + "{}\n"
    buf += "  errs := obj.As(ctx, &data, types.ObjectAsOptions{\n"
    buf += "     UnhandledNullAsEmpty: true,\n"
    buf += "     UnhandledUnknownAsEmpty: true,\n"
    buf += "  })\n"
    buf += "  diags.Append(errs...)\n"
    buf += "  if diags.HasError() {\n return nil\n}\n\n"


    # Populate model attributes
    buf += "  mdl := &ixapi." + type_name + "{}\n"
    for name, prop in props.items():
        required = prop_is_required(component, name)
        if is_readonly_model(type_name):
            required = True
        buf += decode_prop_from_object(name, prop, required)

    buf += "return mdl\n"
    buf += "}\n\n"


    return buf


def prop_is_required(component, name):
    """Check if prop is required"""
    required_props = component.get("required", [])
    return name in required_props


def decode_prop_from_object(name, prop, required):
    """Decode a property"""
    attr_name = idiomize(camelize(name))
    ref_type = ref_type_from_prop(prop)
    prop_type = prop.get("type")
    prop_fmt = prop.get("format")

    if is_readonly_attr(name):
        return ""

    buf = ""
    buf += "if !data." + attr_name + ".Null && "
    buf += "   !data." + attr_name + ".Unknown {\n"
    buf += "   mdl." + attr_name + " = "

    if ref_type:
        buf += ref_type + "FromObject(ctx, diags, data." + attr_name + ")\n"
    else:
        fn = decode_func_from_prop(prop, required)
        if fn:
            buf += fn + "(data." + attr_name + ")\n"
        else:
            if required:
                buf += "data." + attr_name + ".Value\n"
            else:
                buf += "&data." + attr_name + ".Value\n"

    buf += "}\n"

    return buf


def decode_func_from_prop(prop, required):
    """Gets the decoding function from a property"""
    prop_type = prop.get("type")
    prop_fmt = prop.get("format")
    ref_type = ref_type_from_prop(prop)
    array_ref_type = ref_type_from_prop(prop.get("items", {}))

    fn = "MustParse"

    if not required and not ref_type and not array_ref_type:
        fn += "Optional"

    if prop_fmt == "date-time":
        fn += "Time"
    elif prop_fmt == "date":
        fn += "Date"
    elif prop_type == "array":
        tfn = decode_func_type_from_prop_type(prop["items"])
        fn += tfn + "List"

    elif prop_type == "integer":
        fn += "Integer"
    else:
        return None

    return fn

def ref_type_from_prop(prop):
    """Ref type or None"""
    ref = prop.get("$ref")
    if not ref:
       return None

    t = ref.split("/")
    return idiomize(t[len(t)-1])


def decode_prop_from_response(name, prop, required):
    """write decoder func"""
    attr_name = idiomize(camelize(name))
    array_ref_type = ref_type_from_prop(prop.get("items", {}))

    buf = "obj.Attrs[\"" + name + "\"] = "

    if not required and not array_ref_type:
        buf += "Optional"

    buf += decode_func_type_from_prop_type(prop)
    buf += "FromResponse(diags, res." + attr_name + ")\n"

    return buf


def decode_func_type_from_prop_type(prop):
    """get the decode func for types"""
    ref_type = ref_type_from_prop(prop)
    if ref_type:
        return ref_type

    ptype = prop["type"]
    pfmt = prop.get("format")

    if pfmt == "date":
        return "DateString"

    if pfmt == "date-time":
        return "DateTimeString"

    if ptype == "array":
        return decode_func_type_from_prop_type(prop["items"]) + "List"

    if ptype == "object":
        return "Map"
    if ptype == "string":
        return "String"
    if ptype == "integer":
        return "Int"
    if ptype == "boolean":
        return "Bool"

    raise NotImplementedError("missing type", ptype, prop)


def render_model_typemap(name, prop):
    """Render type mapping from property"""
    buf = '"' + name + '": '
    buf += typemap_type_from_prop(prop)
    buf += ",\n"
    return buf


def typemap_type_from_prop(prop):
    """Typemapping property type"""
    ref = prop.get("$ref")
    if ref:
        return typemap_type_ref(ref)

    ptype = prop["type"]
    if ptype == "array":
        return typemap_type_list(prop)
    if ptype == "object":
        return typemap_type_object(prop)
    if ptype == "string":
        return "types.StringType"
    if ptype == "integer":
        return "types.Int64Type"
    if ptype == "boolean":
        return "types.BoolType"

    raise NotImplementedError("missing type", ptype, prop)


def typemap_type_ref(ref):
    """Ref type"""
    t = ref.split("/")
    ref_type = idiomize(t[len(t)-1])

    buf = "types.ObjectType{\n"
    buf += "  AttrTypes: " + ref_type + "Types,\n"
    buf += "}"

    return buf

def typemap_type_object(prop):
    """Object type"""
    buf = "types.MapType{\n"
    buf += " ElemType: types.StringType,\n"
    buf += "}"
    return buf


def typemap_type_list(prop):
    """List Type"""
    items = prop["items"]
    buf = "types.ListType{\n"
    buf += "  ElemType: " + typemap_type_from_prop(items) + ", \n"
    buf += "}"
    return buf


def render_model_attr(name, prop):
    """Render model attribute"""
    description = comment_newlines(
        prop.get("description", f"is a {name}"))

    attr_name = idiomize(camelize(name))
    attr_type = model_type_from_prop(prop)

    buf = "// %s %s\n" % (attr_name, description)
    buf += "%s %s `tfsdk:\"%s\"`\n\n" % (attr_name, attr_type, name)

    return buf


def model_type_from_prop(prop):
    """Get the property type"""
    ref = prop.get("$ref")
    if ref:
        return "types.Object"

    ptype = prop["type"]
    if ptype == "array":
        return "types.List"
    if ptype == "object":
        return "types.Map"
    if ptype == "string":
        return "types.String"
    if ptype == "integer":
        return "types.Int64"
    if ptype == "boolean":
        return "types.Bool"

    raise NotImplementedError("missing type", ptype, prop)


def write_files(spec, pkg_path):
    """Write files to package path"""
    filename = path.join(pkg_path, "models.go")

    models = render_models(spec)
    with open(filename, "w+") as f:
        f.write(PRELUDE)
        f.write("// SchemaVersion is the version of the IX-API schema\n")
        f.write(f"const SchemaVersion = \"{schema_version}\"\n\n")

        f.write(models)

    run(["goimports", "-w", filename], cwd=pkg_path)

