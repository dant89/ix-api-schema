

from os import path
from subprocess import run

from ixapi_schema.codegen.go import dicts
from ixapi_schema.codegen.go.text import (
    idiomize,
    camelize,
    comment_newlines,
)
from ixapi_schema.codegen.go.components import (
    is_writeonly_attr,
    is_readonly_attr,
)
from ixapi_schema.v2 import __version__ as schema_version


PRELUDE = """package schemas

import (
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

//
// CAUTION:
//   This file is generated from the IX-API
//   openapi specs. DO NOT EDIT.
//

"""

def models_from_components(components):
    """Prepares models for rendering"""
    models = {}

    excluded = [
        "AuthToken",
    ]

    # Skip all Request and Update types, the will be merged
    for name, comp in components.items():
        if name in excluded:
            continue
        if name.endswith("Request"):
            continue
        if name.endswith("Update"):
            continue
        if name.endswith("Patch"):
            continue
        models[name] = comp

    # Combine resource response model, e.g. Account
    # with update and request model
    for name, model in models.items():
        update = components.get(name + "Patch", {})
        request = components.get(name + "Request", {})
        models[name] = dicts.merge(request, update, model)

        # Use required fields only from request / create
        # as updates are handled using the patch operation.
        models[name]["required"] = request.get("required", [])

    return models


def prop_is_required(component, name):
    """Check if prop is required"""
    required_props = component.get("required", [])
    return name in required_props


def ref_type_from_prop(prop):
    """Ref type or None"""
    ref = prop.get("$ref")
    if not ref:
       return None

    t = ref.split("/")
    return idiomize(t[len(t)-1])


def model_attr_schema_required(component, pname):
    required = prop_is_required(component, pname)
    readonly = is_readonly_attr(component, pname)

    buf = ""
    # Set required / optional
    if required and readonly:
        buf += "Computed: true,\n"
    elif required:
        buf += "Required: true,\n"
    else:
        buf += "Optional: true,\n"
        buf += "Computed: true,\n"
    return buf


def model_attr_schema_type(ptype):
    """Schema type from prop"""
    types = {
        "array": "schema.TypeList",
        "string": "schema.TypeString",
        "integer": "schema.TypeInt",
        "boolean": "schema.TypeBool",
        "number": "schema.TypeFloat",
    }
    return types[ptype]


def model_attr_schema(component, pname):
    """model schema"""
    prop = component["properties"][pname]
    stype = model_attr_schema_type(prop["type"])
    description = prop.get("description", "") \
        .replace("\n", " ") \
        .replace("\"", "\\\"")

    buf = ""
    buf += "&schema.Schema{\n"
    buf += "  Type: " + stype + ",\n"
    buf += model_attr_schema_required(component, pname)

    if description != "":
        buf += "  Description: \"" + description + "\",\n"

    buf += "},\n"
    return buf


def model_attr_schema_list(component, pname):
    """schema.typeList, references object"""
    prop = component["properties"][pname]
    ref_type = ref_type_from_prop(prop["items"])
    items_type = prop["items"].get("type")

    elem = ""
    if items_type == None and ref_type != None:
        elem += "Elem: &schema.Resource{\n"
        elem += "   Schema: " + ref_type + "Schema(),\n"
        elem += "},\n"
    elif items_type != None:
        elem += "Elem: &schema.Schema{\n"
        elem += "   Type: " + model_attr_schema_type(items_type) + ",\n"
        elem += "},"

    buf = ""
    buf += "&schema.Schema{\n"
    buf += "  Type: schema.TypeList,\n"
    buf += model_attr_schema_required(component, pname)
    buf += elem
    buf += ""
    buf += "},\n"

    return buf


def model_attr_schema_embedded(component, pname):
    """schema.typeList, references object"""
    prop = component["properties"][pname]
    ref_type = ref_type_from_prop(prop)
    buf = ""
    buf += "&schema.Schema{\n"
    buf += "  Type: schema.TypeList,\n"
    buf += "  MaxItems: 1,\n"
    buf += model_attr_schema_required(component, pname)
    buf += "  Elem: &schema.Resource{\n"
    buf += "    Schema: " + ref_type + "Schema(),\n" # new schema instance
    buf += "  },\n"
    buf += "},\n"
    return buf



def model_attr_prop_schema(component, pname):
    """Render a property schema"""
    prop = component["properties"][pname]
    ref_type = ref_type_from_prop(prop)
    if ref_type:
        return model_attr_schema_embedded(component, pname)

    ptype = prop["type"]

    if ptype == "object":
        return None

    if ptype == "array":
        return model_attr_schema_list(component, pname)

    return model_attr_schema(component, pname)


def model_attr(component, pname):
    """Render a model property"""
    if pname == "vlan_type":
        return ""
    if pname == "type": # polymorphic type
        return ""

    # Replace reserved attribute names
    attr_name = pname
    if attr_name == "connection":
        attr_name = "network_connection"

    schema = model_attr_prop_schema(component, pname)
    if not schema:
        return ""

    buf = "\"" + attr_name + "\": " + schema + "\n"
    return buf


def model_schema(component):
    """Render the model schema"""
    props = component["properties"]

    buf = "return map[string]*schema.Schema{\n"
    for pname in props:
        buf += model_attr(component, pname)
    buf += "}\n"
    return buf


def render_model(name, component):
    """Render a single model component"""
    type_name = idiomize(name)
    description = comment_newlines(
        component.get("description", f"is {name}"))

    component["name"] = name
    component["type_name"] = type_name

    props = component.get("properties")
    if not props:
        print(f"skipping {name}, no properties")
        return ""

    buf = ""

    # We start with the schema
    buf += "// " + type_name + "Schema is the terraform schema for the model\n"
    buf += "func " + type_name + "Schema() map[string]*schema.Schema{\n"
    buf += model_schema(component)
    buf += "}\n\n"

    return buf


def render_models(spec):
    """Render all models from openapi spec"""
    buf = ""
    components = spec["components"]["schemas"]

    models = models_from_components(components)

    for name, component in models.items():
        buf += render_model(name, component)
    return buf


def write_files(spec, pkg_path):
    """Write files to package path"""
    filename = path.join(pkg_path, "schemas.go")

    models = render_models(spec)
    with open(filename, "w+") as f:
        f.write(PRELUDE)
        f.write("// SchemaVersion is the version of the IX-API schema\n")
        f.write(f"const SchemaVersion = \"{schema_version}\"\n\n")
        f.write(models)

    run(["goimports", "-w", filename], cwd=pkg_path)


