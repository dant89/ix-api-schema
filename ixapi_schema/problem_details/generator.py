
"""
Generate documentation pages for RFC7807 Problem Details.

These pages can be served directly or exported to
an output directory to be served as static files.
"""

import re
import pathlib

import jinja2

from ixapi_schema.problem_details import reflection


# Configure templating
jinja_env = jinja2.Environment(
    loader=jinja2.PackageLoader("ixapi_schema.problem_details", "templates"),
    autoescape=False, # Be careful!
    trim_blocks=True,
    lstrip_blocks=True)


def _slugify(string):
    """Slugify text"""
    return "-".join(t.lower() for t
                    in re.findall("[A-Z][^A-Z]*", string))


def _make_problem_repr(problem):
    """Make a renderable representation of the problem"""
    p_inst = problem()
    name = problem.__name__.replace("Problem", "")
    return {
        "name": name,
        "title": problem.default_title,
        "status": problem.default_status,
        "status_code": problem.default_response_status,
        "detail": problem.default_detail,
        "description": problem.__doc__,
        "extra": problem.default_extra,
        "url": "{}.html".format(p_inst.ptype),
    }


def render_index(problems):
    """Render problems index.html"""
    problem_classes = reflection.find_problems(problems)
    problems = [_make_problem_repr(problem)
                for problem in problem_classes]
    tmpl = jinja_env.get_template("problems/index.html")

    return tmpl.render({"problems": problems})


def render_details(problem):
    """Render problem details page"""
    tmpl = jinja_env.get_template("problems/problem.html")
    return tmpl.render({
        "problem": _make_problem_repr(problem),
    })


def export(problems, output_path):
    """Export all to an output put"""
    output = pathlib.Path(output_path)
    output.mkdir(parents=True, exist_ok=True) # mkdir -p

    problem_classes = reflection.find_problems(problems)

    # index.html
    with open(output.joinpath("index.html"), "w") as f:
        f.write(render_index(problems))

    # all problems
    for problem_cls in problem_classes:
        problem = problem_cls()
        filename = problem.ptype + ".html" 
        with open(output.joinpath(filename), "w") as f:
            f.write(render_details(problem_cls))

