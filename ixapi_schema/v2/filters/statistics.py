
from ixapi_schema.openapi import components


TIMESERIES_FILTER_SET = components.FilterSet({
    "start": components.DateTimeFilter(
        label="""
            Start of the timeseries.
        """
    ),
    "end": components.DateTimeFilter(
        label="""
            End of the timeseries.

            Default: `now`
        """
    ),
    "fields": components.CharFilter(label="""
        Select fields from the aggregates to export as a timeseries.

        Default: `average_pps_in,average_pps_out,average_ops_in,average_ops_out`
    """)
})

