
"""
Api V2 Authentication
"""

from ixapi_schema.v2.entities import auth, problems

PATHS = {
    "/auth/token": {
        "description": """
            **DEPRECATION NOTICE:**
            Providing the method of authentication is up to the implementer.
            We suggest using OAuth 2.

            Open an authorized session with the IX-API. This is equivalent to
            a 'login' endpoint.

            Upon success, you will receive an `access_token` and
            a `refreh_token`. Both are JWTs and have limited lifetimes. You
            can get information like their expiry time directly by parsing
            the tokens.

            Use the `access_token` as `Bearer` in your `Authorization` header
            for getting access to the API.

            When the session (i.e. the `access_token`) expires, use the
            `refresh_token` (if it's not yet expired) with the
            `refresh`-endpoint to reauthenticate and get a fresh `access_token`.
        """,
        "POST": {
            "description": """
                Authenticate an API user identified by `api_key` and
                `api_secret`.
            """,
            "security": [],
            "operation": "auth_token_create",
            "request": auth.AuthTokenRequest(),
            "responses": {
                201: auth.AuthToken(),
            },
            "problems": [
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotAuthenticatedProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
    },
    "/auth/refresh": {
        "POST": {
            "description": """
                Reauthenticate the API user, issue a new `access_token`
                and `refresh_token` pair by providing the `refresh_token`
                in the request body.
            """,
            "security": [],
            "operation": "auth_token_refresh",
            "request": auth.RefreshTokenRequest(),
            "responses": {
                201: auth.AuthToken(),
            },
            "problems": [
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotAuthenticatedProblem(),
                problems.ValidationErrorProblem(),
            ],
        },
    },
}
