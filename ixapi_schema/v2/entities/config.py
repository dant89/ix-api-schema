
"""
Service Access
--------------

Serialize access objects like connections or ports,
network service access objects.
"""

import enum

from ixapi_schema.coupling import qs
from ixapi_schema.openapi import components
from ixapi_schema.v2.entities import crm, events, cancellation
from ixapi_schema.v2.constants.config import (
    PortState,
    ConnectionMode,
    LACPTimeout,
    RouteServerSessionMode,
    ConnectionMode,
    BGPSessionType,
    VLanEthertype,
    VLanType,
    P2MPRole,

    NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN,
    NETWORK_SERVICE_CONFIG_TYPE_P2P,
    NETWORK_SERVICE_CONFIG_TYPE_MP2MP,
    NETWORK_SERVICE_CONFIG_TYPE_P2MP,
    NETWORK_SERVICE_CONFIG_TYPE_CLOUD,
    NETWORK_SERVICE_CONFIG_ENTITIES,
    NETWORK_SERVICE_CONFIG_TYPES,

    NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER,
    NETWORK_FEATURE_CONFIG_ENTITIES,
    NETWORK_FEATURE_CONFIG_TYPES,

    VLAN_CONFIG_TYPE_DOT1Q,
    VLAN_CONFIG_TYPE_QINQ,
    VLAN_CONFIG_TYPE_PORT,
    VLAN_CONFIG_ENTITIES,

    P2MP_ROLE_LEAF,
)

#
# Connections
#
class ConnectionBase(
        crm.OwnableBase,
        crm.InvoiceableBase,
        crm.ContactableBase,
        components.Component,
    ):
    """Connection Base"""
    mode = components.EnumField(
        ConnectionMode,
        help_text="""
            Sets the mode of the connection. The mode can be:

            - `lag_lacp`: connection is build as a LAG with LACP enabled
            - `lag_static`: connection is build as LAG with static configuration
            - `flex_ethernet`: connect is build as a FlexEthernet channel
            - `standalone`: only one port is allowed in this connection without
            any bundling.

            Example: "lag_lacp"
        """)
    lacp_timeout = components.EnumField(
        LACPTimeout,
        required=False,
        default=None,
        allow_null=True,
        help_text="""
            This sets the LACP Timeout mode. Both ends of the connections need
            to be configured the same.

            Example: "slow"
        """)

    product_offering = components.PrimaryKeyRelatedField(
        help_text="""
        The product offering must match the type `connection`.
        """,
        queryset=qs.Any("catalog.ProductOffering"))


class ConnectionRequest(ConnectionBase):
    """Request a new connection"""
    # Both are only valid when requesting a port...
    port_quantity = components.IntegerField(
        min_value=1,
        required=True,
        help_text="""
            The number of `PortReservation`s that will be
            created for this connection.
        """)

    subscriber_side_demarcs = components.ListField(
        child=components.CharField(),
        required=False,
        help_text="""
            The workflow for allocating ports is dependent on the
            `cross_connect_initiator` property of the
            `product_offering`:

            **Cross-Connect initiator: exchange**

            The subscriber needs to provide a
            list of demarc information.

            At least one needs to be provided, but not more than
            `port_quantity`.

            The content is interpreted by the exchange and may
            contain a reference to a pre-existing cross connect order
            or information required for patching in a structured
            format (e.g.
            `<pp-identifier>.<hu-identifier>.<slot-identifier>.<port-identifier>`).

            Please refer to the usage guide of the internet exchange.

            ---

            **Cross-Connect initiator: subscriber**

            This field can be omitted, when the cross connect
            initiator is the `subscriber`.
        """)

    connecting_party = components.CharField(
        allow_null=True,
        required=False,
        help_text="""
            Name of the service provider who establishes
            connectivity on your behalf.

            This is only relevant, if the cross connect initiator
            is the `subscriber` and might be `null`.

            Please refer to the usage guide of the internet exchange.
        """)


class Connection(
        events.StatefulBase,
        cancellation.CancelableBase,
        ConnectionBase):
    """Connection"""
    id = components.CharField(max_length=80)
    name = components.CharField(max_length=80,
                                required=True)

    ports = components.PrimaryKeyRelatedField(
        many=True,
        required=False,
        queryset=qs.Any("config.Port"),
        help_text="""
            References to the port belonging to this connection. Typically
            all ports within one connection are distributed over the same
            device.

            Example: ["ID23", "42", "5"]
        """)

    port_reservations = components.PrimaryKeyRelatedField(
        many=True,
        required=False,
        queryset=qs.Any("config.PortReservation"),
        help_text="""
            A list of `port-reservations` for this connection.
        """)

    pop = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.PointOfPresence"),
        help_text="""
            The ID of the point of presence (see `/pops`), where
            the physical port(s) are present.

            Example: "pop:127388:LD3"
        """)

    speed = components.IntegerField(
        allow_null=True,
        required=False,
        min_value=0,
        help_text="""
            Shows the total bandwidth of the connection in Mbit/s.

            Example: 20000
        """)

    capacity_allocated = components.IntegerField(
        min_value=0,
        required=True,
        help_text="""
            Sum of the bandwidth of all network services using
            the connection in Mbit/s.
        """)

    capacity_allocation_limit = components.IntegerField(
        allow_null=True,
        required=True,
        help_text="""
            Maximum allocatable capacity of the connection in Mbit/s.
            When `null`, the exchange does not impose any limit.

            Example: 25000
        """)

    vlan_types = components.ListField(
        read_only=True,
        help_text="""
            A list of vlan config types you can configure using
            this connection.
        """,
        default=[
            VLanType.PORT.value,
            VLanType.DOT1Q.value,
        ],
        child=components.EnumField(VLanType))

    outer_vlan_ethertypes = components.ListField(
        read_only=True,
        help_text="""
            The ethertype of the outer tag in hexadecimal notation.

            Example: ["0x8100"]
        """,
        child=components.EnumField(
            VLanEthertype,
            default=VLanEthertype.E_0x8100.value))

    port_quantity = components.IntegerField(
        min_value=1,
        required=True,
        help_text="""
            The number of ports which should be allocated
            for this connection.
        """)

    subscriber_side_demarcs = components.ListField(
        child=components.CharField(),
        required=False,
        help_text="""
            The workflow for allocating ports is dependent on the
            `cross_connect_initiator` property of the
            `product_offering`:

            **Cross-Connect initiator: exchange**

            The subscriber needs to provide a
            list of demarc information.


            At least one needs to be provided, but not more than
            `port_quantity`.

            The content is interpreted by the exchange and may
            contain a reference to a pre-existing cross connect order
            or information required for patching in a structured
            format (e.g.
            `<pp-identifier>.<hu-identifier>.<slot-identifier>.<port-identifier>`).

            Please refer to the usage guide of the internet exchange.

            ---

            **Cross-Connect initiator: subscriber**

            This field can be omitted, when the cross connect
            initiator is the `subscriber`.
        """)



class ConnectionUpdate(ConnectionBase):
    """Connection Update"""


class ConnectionPatch(ConnectionBase):
    """Connection Update"""


class PortReservationBase(
        crm.ContractableBase,
        crm.ExternalReferenceBase,
        components.Component,
    ):
    """A PortReservation"""
    # Patch information
    subscriber_side_demarc = components.CharField(
        required=False,
        help_text="""
            In an exchange initiated scenario, this field will
            indicated one of the provided `subscriber_side_demarcs`
            from the connection.
        """)

    connecting_party = components.CharField(
        allow_null=True,
        required=False,
        help_text="""
            Name of the service provider who establishes
            connectivity on your behalf.

            This is only relevant, if the cross connect initiator
            is the `subscriber`.

            Please refer to the usage guide of the internet exchange.
        """)

    cross_connect_id = components.CharField(
        allow_null=False,
        required=False,
        help_text="""
            An optional identifier of a cross connect.
        """)


class PortReservation(
        events.StatefulBase,
        cancellation.CancelableBase,
        PortReservationBase):
    """A PortReservation"""
    id = components.CharField()

    connection = components.PrimaryKeyRelatedField(
        required=True,
        help_text="""
            The `Port` will become part of this connection.
        """,
        queryset=qs.Any("config.Connection"))

    exchange_side_demarc = components.CharField(
        allow_null=True,
        required=False,
        help_text="""
            Exchange side demarc information. This field will only
            be filled in when the port state is `allocated` or
            in `production`.

            Otherwise this field will be `null`.
        """)

    port = components.PrimaryKeyRelatedField(
        allow_null=True,
        required=False,
        help_text="""
            This field will be null, until a port will
            be allocated.
        """,
        queryset=qs.Any("config.Port"))



class PortReservationRequest(PortReservationBase):
    """A PortReservation"""
    connection = components.PrimaryKeyRelatedField(
        required=True,
        help_text="""
            A connection is required for port allocation.
        """,
        queryset=qs.Any("config.Connection"))


class PortReservationUpdate(PortReservationBase):
    """PortReservation Update"""


class PortReservationPatch(PortReservationBase):
    """PortReservation Update"""


#
# Device Ports
#
class PortBase(
        crm.OwnableBase,
        crm.InvoiceableBase,
        crm.ContactableBase,
        components.Component,
    ):
    """Port Base"""
    connection = components.PrimaryKeyRelatedField(
        allow_null=True,
        required=False,
        queryset=qs.Any("config.Connection"))

    speed = components.IntegerField(
        read_only=True,
        min_value=0,
        default=None,
        allow_null=True)


class Port(
        events.StatefulBase,
        PortBase,
    ):
    """Port"""
    id = components.CharField(max_length=80)
    name = components.CharField(
        max_length=80,
        read_only=True,
        default="",
        help_text="""
            Name of the port (set by the exchange)
        """)
    media_type = components.CharField(
        max_length=20,
        help_text="""
            The media type of the port.
            Query the device's capabilities for available types.

            Example: "10GBASE-LR"
        """)

    # State
    operational_state = components.EnumField(
        PortState,
        required=False,
        help_text="""
            The operational state of the port.
        """)


    # Relations
    device = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.Device"),
        help_text="""
            The device the port.

            Example: "device:29139871"
        """)

    pop = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.PointOfPresence"),
        help_text="""
            Same as the `pop` of the `device`.

            Example: "pop:2913"
        """)


#
# Network Service VLan Config
#
class VLanConfigQinQ(components.Component):
    """A QinQ vlan configuration"""
    outer_vlan = components.IntegerField(
        min_value=1,
        max_value=4094,
        allow_null=True,
        required=False,
        help_text="""
            The outer VLAN id.
            If `null`, the IXP will auto-select
            a valid vlan-id.

            Example: 200
        """
    )

    # FIXME: Hex notation is not possible in json, so the
    # documentation is wrong here.
    outer_vlan_ethertype = components.EnumField(
        VLanEthertype,
        default=VLanEthertype.E_0x8100.value,
        help_text="""
            The ethertype of the outer tag in hexadecimal notation.
        """
    )
    inner_vlan = components.IntegerField(
        min_value=1,
        max_value=4094,
        help_text="""
            The inner VLAN id.

            Example: 200
        """
    )

    __polymorphic__ = "VlanConfig"
    __polymorphic_on__ = "vlan_type"
    __polymorphic_type__ = VLAN_CONFIG_TYPE_DOT1Q


class VLanConfigDot1Q(components.Component):
    """A Dot1Q vlan configuration"""
    vlan = components.IntegerField(
        min_value=1,
        max_value=4094,
        required=False,
        allow_null=True,
        help_text="""
        A VLAN tag. If `null`, the IXP will auto-select
        a valid vlan-id.

        Example: 23
        """
    )

    vlan_ethertype = components.EnumField(
        VLanEthertype,
        default=VLanEthertype.E_0x8100.value,
        help_text="""
            The ethertype of the vlan in hexadecimal notation.
        """
    )

    __polymorphic__ = "VlanConfig"
    __polymorphic_type__ = VLAN_CONFIG_TYPE_DOT1Q
    __polymorphic_on__ = "vlan_type"


class VLanConfigPort(components.Component):
    """A Port vlan configuration"""
    __polymorphic__ = "VlanConfig"
    __polymorphic_type__ = VLAN_CONFIG_TYPE_PORT
    __polymorphic_on__ = "vlan_type"


class VlanConfig(components.PolymorphicComponent):
    """
    The vlan configuration defines how the service
    is made available on the connection.
    """
    serializer_classes = {
        VLAN_CONFIG_TYPE_DOT1Q: VLanConfigDot1Q,
        VLAN_CONFIG_TYPE_QINQ: VLanConfigQinQ,
        VLAN_CONFIG_TYPE_PORT: VLanConfigPort,
    }

    entity_types = VLAN_CONFIG_ENTITIES

    __polymorphic_on__ = "vlan_type"


#
# Network Service Configs
#
class RateLimitedNetworkServiceConfigBase(components.Component):
    """
    Rate limited network services include a capacity
    property.
    """
    capacity = components.IntegerField(
        required=False,
        default=None,
        min_value=1,
        allow_null=True,
        help_text="""
            The capacity of the service in Mbps. If set to Null,
            the maximum capacity will be used, i.e. the virtual circuit is
            not rate-limited.

            An exchange may choose to constrain the available capacity range
            of a `ProductOffering`.

            That means, the service can consume up to the total bandwidth
            of the `Connection`.

            Typically the service is charged based on the capacity.
        """)
#
# Network Service Configs
#
class NetworkServiceConfigBase(
        crm.OwnableBase,
        crm.InvoiceableBase,
        crm.ContactableBase,
        components.Component,
    ):
    """Network Service Config"""
    connection = components.PrimaryKeyRelatedField(
        queryset=qs.Any("config.Connection"),
        help_text="""
            The id of the connection to use for this `NetworkServiceConfig`. 
        """,
    )

    network_feature_configs = components.PrimaryKeyRelatedField(
        many=True,
        read_only=True,
        help_text="""
            A list of ids of `NetworkFeatureConfig`s.

            Example: ["12356", "43829"]
        """)

    vlan_config = VlanConfig()


class NetworkServiceConfigRequestBase(
        crm.OwnableBase,
        components.Component
    ):
    """Network Service Config Request"""
    network_service = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkService"),
        help_text="""
            The id of the `NetworkService` to configure.
        """)

    __polymorphic__ = "NetworkServiceConfigRequest"


class NetworkServiceConfigUpdateBase(
        crm.OwnableBase,
        components.Component,
    ):
    """Network Service Config Update"""
    vlan_config = VlanConfig()

    __polymorphic__ = "NetworkServiceConfigUpdate"


class NetworkServiceConfigOutputBase(
        events.StatefulBase,
        cancellation.CancelableBase,
        components.Component,
    ):
    """Network Service Config"""
    id = components.CharField(max_length=80)

    network_service = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkService"),
        help_text="""
            The id of the configured network service.
        """)

    __polymorphic__ = "NetworkServiceConfig"

# Exchange Lan

class ExchangeLanNetworkServiceConfigBase(
        NetworkServiceConfigBase,
        RateLimitedNetworkServiceConfigBase,
    ):
    """Exchange Lan Network Service Config"""
    asns = components.ListField(
        help_text="""
            A list of AS numbers.

            Depending on the implementation, these can be used for different
            purposes.  For example in the members list on the website, links to
            the looking glass or even generating IPv6 prefixes.
        """,
        child=components.IntegerField(min_value=0, max_value=4294967295),
        default=[],
        min_length=0,
        max_length=20)

    macs = components.PrimaryKeyRelatedField(
        source="mac_addresses",
        required=False,
        queryset=qs.Any("ipam.MacAddress"),
        many=True,
        help_text="""
            A list of mac-address IDs.
        """)

    ips = components.PrimaryKeyRelatedField(
        source="ip_addresses",
        many=True,
        read_only=True,
        help_text="""
            A list of ip-address IDs.

            Allocation of IP Addresses might be deferred depending on
            the IXP implementation. No assumption should be made.
        """)

    listed = components.BooleanField(
        help_text="The customer wants to be featured on the member list")

    availability_zone = components.PrimaryKeyRelatedField(
        required=False,
        allow_null=True,
        queryset=qs.Any("catalog.AvailabilityZone"),
        help_text="""
            The availability zone that shall be used on the provider side.

            Availability Zones may not be supported for exchange_lan because by
            default they span multiple networks.

            If an availability zone is set then this refers to a circuit that
            is placed on a specific on-ramp to the exchange_lan.
        """)

    __polymorphic_type__ = \
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN


class ExchangeLanNetworkServiceConfigRequest(
        NetworkServiceConfigRequestBase,
        ExchangeLanNetworkServiceConfigBase,
    ):
    """Exchange Lan Network Service Config Request"""
    product_offering = components.PrimaryKeyRelatedField(
        help_text="""
        The product offering must match the type `exchange_lan`
        and must refer to the related network service through
        the `exchange_lan_network_service` property.
        """,
        queryset=qs.Any("catalog.ProductOffering"))


class ExchangeLanNetworkServiceConfigUpdate(
        NetworkServiceConfigUpdateBase,
        ExchangeLanNetworkServiceConfigBase,
    ):
    """Exchange Lan Network Service Config Update"""


class ExchangeLanNetworkServiceConfigPatch(ExchangeLanNetworkServiceConfigUpdate):
    """Exchange Lan Network Service Config Update"""
    __polymorphic__ = "NetworkServiceConfigPatch"


class ExchangeLanNetworkServiceConfig(
        NetworkServiceConfigOutputBase,
        ExchangeLanNetworkServiceConfigBase,
    ):
    """Exchange Lan Network Service Config"""
    product_offering = components.PrimaryKeyRelatedField(
        help_text="""
        The product offering must match the type `exchange_lan`
        and must refer to the related network service through
        the `exchange_lan_network_service` property.
        """,
        queryset=qs.Any("catalog.ProductOffering"))


class EVPNetworkServiceConfigBase(components.Component):
    """Ethernet Virtual Private Network Service Config"""
    product_offering = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.ProductOffering"),
        required=False,
        help_text="""
            An optional id of a `ProductOffering`.

            Valid ids of product-offerings can be found in the
            `nsc_product_offerings` property of the `NetworkService`.
        """)

# P2P

class P2PNetworkServiceConfigBase(
        NetworkServiceConfigBase,
        EVPNetworkServiceConfigBase,
        RateLimitedNetworkServiceConfigBase,
    ):
    """P2P Network Service Config"""
    __polymorphic_type__ = \
        NETWORK_SERVICE_CONFIG_TYPE_P2P


class P2PNetworkServiceConfigRequest(
        NetworkServiceConfigRequestBase,
        P2PNetworkServiceConfigBase,
    ):
    """P2P Network Service Config Request"""


class P2PNetworkServiceConfigUpdate(
        NetworkServiceConfigUpdateBase,
        P2PNetworkServiceConfigBase,
    ):
    """P2P Network Service Config Update"""


class P2PNetworkServiceConfigPatch(P2PNetworkServiceConfigUpdate):
    """P2P Network Service Config Update"""
    __polymorphic__ = "NetworkServiceConfigPatch"


class P2PNetworkServiceConfig(
        NetworkServiceConfigOutputBase,
        P2PNetworkServiceConfigBase,
    ):
    """P2P Network Service Config"""


# P2MP
class P2MPNetworkServiceConfigBase(
        EVPNetworkServiceConfigBase,
        RateLimitedNetworkServiceConfigBase,
        NetworkServiceConfigBase,
    ):
    """P2MP Network Service Config"""
    #macs = components.PrimaryKeyRelatedField(
    #    source="mac_addresses",
    #    required=False,
    #    queryset=qs.Any("ipam.MacAddress"),
    #    many=True)

    role = components.EnumField(
        P2MPRole,
        required=False,
        default=P2MP_ROLE_LEAF,
        help_text="""
            A `leaf` can only reach roots and is
            isolated from other leafs. A `root` can
            reach any other point in the virtual circuit
            including other roots.
        """)

    __polymorphic_type__ = \
        NETWORK_SERVICE_CONFIG_TYPE_P2MP


class P2MPNetworkServiceConfigRequest(
        NetworkServiceConfigRequestBase,
        P2MPNetworkServiceConfigBase,
    ):
    """P2MP Network Service Config Request"""


class P2MPNetworkServiceConfigUpdate(
        P2MPNetworkServiceConfigBase,
        NetworkServiceConfigUpdateBase,
    ):
    """P2MP Network Service Config Update"""


class P2MPNetworkServiceConfigPatch(P2MPNetworkServiceConfigUpdate):
    """P2MP Network Service Config Update"""
    __polymorphic__ = "NetworkServiceConfigPatch"


class P2MPNetworkServiceConfig(
        P2MPNetworkServiceConfigBase,
        NetworkServiceConfigOutputBase,
    ):
    """P2MP Network Service Config"""

# MP2MP

class MP2MPNetworkServiceConfigBase(
        EVPNetworkServiceConfigBase,
        RateLimitedNetworkServiceConfigBase,
        NetworkServiceConfigBase,
    ):
    """MP2MP Network Service Config"""
    macs = components.PrimaryKeyRelatedField(
        help_text="""
            A list of MAC address IDs. You have to register the
            address using the `macs_create` operation.
        """,
        source="mac_addresses",
        required=False,
        queryset=qs.Any("ipam.MacAddress"),
        many=True)

    ips = components.PrimaryKeyRelatedField(
        source="ip_addresses",
        many=True,
        read_only=True,
        help_text="""
            A list of ip-address IDs.

            Allocation of IP Addresses might be deferred depending on
            the IXP implementation. No assumption should be made.
        """)

    asns = components.ListField(
        help_text="""
            A list of AS numbers.

            Depending on the implementation, these can be used for different
            purposes.  For example in the members list on the website, links to
            the looking glass or even generating IPv6 prefixes.
        """,
        required=False,
        child=components.IntegerField(min_value=0, max_value=4294967295),
        default=[],
        min_length=0,
        max_length=20)

    __polymorphic_type__ = \
        NETWORK_SERVICE_CONFIG_TYPE_MP2MP


class MP2MPNetworkServiceConfigRequest(
        NetworkServiceConfigRequestBase,
        MP2MPNetworkServiceConfigBase,
    ):
    """MP2MP Network Service Config Request"""


class MP2MPNetworkServiceConfigUpdate(
        NetworkServiceConfigUpdateBase,
        MP2MPNetworkServiceConfigBase,
    ):
    """MP2MP Network Service Config Update"""


class MP2MPNetworkServiceConfigPatch(MP2MPNetworkServiceConfigUpdate):
    """MP2MP Network Service Config Update"""
    __polymorphic__ = "NetworkServiceConfigPatch"


class MP2MPNetworkServiceConfig(
        NetworkServiceConfigOutputBase,
        MP2MPNetworkServiceConfigBase,
    ):
    """MP2MP Network Service Config"""


# Cloud

class CloudNetworkServiceConfigBase(
        NetworkServiceConfigBase,
    ):
    """Cloud Network Service Config"""
    handover = components.IntegerField(
        required=True,
        min_value=1,
        help_text="""
            The handover enumerates the connection and is
            required for checking diversity constraints.

            It must be within `1 <= x <= network_service.diversity`.

            Example: 1
        """
    )

    cloud_vlan = components.IntegerField(
        allow_null=True,
        min_value=1,
        max_value=4094,
        help_text="""
            If the `provider_vlans` property of the `ProductOffering` is
            `multi`, a numeric value refers to a specific vlan on the service
            provider side.

            Otherwise, if set to `null`, it refers to all unmatched
            vlan ids on the service provider side. (All vlan ids from the
            service provider side are presented as tags within any vlans specified
            in `vlan_config`.)

            If the `provider_vlans` property of the `ProductOffering` is `single`,
            the `cloud_vlan` MUST be `null` or MUST NOT be provided.
        """,
    )

    availability_zone = components.PrimaryKeyRelatedField(
        required=False,
        allow_null=True,
        queryset=qs.Any("catalog.AvailabilityZone"),
        help_text="""
            The availability zone that shall be used on the provider side.
        """)

    __polymorphic_type__ = \
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD


class CloudNetworkServiceConfigRequest(
        NetworkServiceConfigRequestBase,
        CloudNetworkServiceConfigBase,
    ):
    """Cloud Network Service Config Request"""


class CloudNetworkServiceConfigUpdate(
        NetworkServiceConfigUpdateBase,
        CloudNetworkServiceConfigBase,
    ):
    """Cloud Network Service Config Update"""


class CloudNetworkServiceConfigPatch(CloudNetworkServiceConfigUpdate):
    """Cloud Network Service Config Update"""
    __polymorphic__ = "NetworkServiceConfigPatch"


class CloudNetworkServiceConfig(
        NetworkServiceConfigOutputBase,
        CloudNetworkServiceConfigBase,
    ):
    """Cloud Network Service Config"""


class NetworkServiceConfigRequest(components.PolymorphicComponent):
    """Polymorhic Network Service Config Request"""
    serializer_classes = {
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkServiceConfigRequest,
        NETWORK_SERVICE_CONFIG_TYPE_P2P:
            P2PNetworkServiceConfigRequest,
        NETWORK_SERVICE_CONFIG_TYPE_P2MP:
            P2MPNetworkServiceConfigRequest,
        NETWORK_SERVICE_CONFIG_TYPE_MP2MP:
            MP2MPNetworkServiceConfigRequest,
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
            CloudNetworkServiceConfigRequest,
    }

    entity_types = NETWORK_SERVICE_CONFIG_ENTITIES


class NetworkServiceConfigUpdate(components.PolymorphicComponent):
    """Polymorphic Network Service Config"""
    serializer_classes = {
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkServiceConfigUpdate,
        NETWORK_SERVICE_CONFIG_TYPE_P2P:
            P2PNetworkServiceConfigUpdate,
        NETWORK_SERVICE_CONFIG_TYPE_P2MP:
            P2MPNetworkServiceConfigUpdate,
        NETWORK_SERVICE_CONFIG_TYPE_MP2MP:
            MP2MPNetworkServiceConfigUpdate,
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
            CloudNetworkServiceConfigUpdate,
    }

    entity_types = NETWORK_SERVICE_CONFIG_ENTITIES


class NetworkServiceConfigPatch(components.PolymorphicComponent):
    """Polymorphic Network Service Config"""
    serializer_classes = {
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkServiceConfigPatch,
        NETWORK_SERVICE_CONFIG_TYPE_P2P:
            P2PNetworkServiceConfigPatch,
        NETWORK_SERVICE_CONFIG_TYPE_P2MP:
            P2MPNetworkServiceConfigPatch,
        NETWORK_SERVICE_CONFIG_TYPE_MP2MP:
            MP2MPNetworkServiceConfigPatch,
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
            CloudNetworkServiceConfigPatch,
    }

    entity_types = NETWORK_SERVICE_CONFIG_ENTITIES


class NetworkServiceConfig(components.PolymorphicComponent):
    """Polymorphic Network Service Config"""
    serializer_classes = {
        NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkServiceConfig,
        NETWORK_SERVICE_CONFIG_TYPE_P2P:
            P2PNetworkServiceConfig,
        NETWORK_SERVICE_CONFIG_TYPE_P2MP:
            P2MPNetworkServiceConfig,
        NETWORK_SERVICE_CONFIG_TYPE_MP2MP:
            MP2MPNetworkServiceConfig,
        NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
            CloudNetworkServiceConfig,
    }

    entity_types = NETWORK_SERVICE_CONFIG_ENTITIES


class IXPSpecificFeatureFlagConfig(components.Component):
    """IXP-Specific Feature Flag Configuration"""
    name = components.CharField(
        max_length=40,
        help_text="""
            The name of the feature flag.

            Example: RPKI-HARD-FILTER
        """)

    enabled = components.BooleanField(
        help_text="""
            Enable the feature.

            *Mandatory features can not be disabled*.
        """)

#
# Feature Configurations
#
class NetworkFeatureConfigBase(
        crm.OwnableBase,
        crm.InvoiceableBase,
        crm.ContactableBase,
        components.Component,
    ):
    """A feature access base serializer"""
    network_feature = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkFeature"))
    network_service_config = components.PrimaryKeyRelatedField(
        queryset=qs.Any("config.NetworkServiceConfig"))

    flags = IXPSpecificFeatureFlagConfig(
        many=True,
        required=False,
        help_text="""
            A list of IXP specific feature flag configs. This can be used
            to enable or disable a specific feature flag.
        """)

    __polymorphic__ = "NetworkFeatureConfig"


class NetworkFeatureConfigUpdateBase(crm.OwnableBase, components.Component):
    """Network Feature Config Updates Base"""
    __polymorphic__ = "NetworkFeatureConfigUpdate"


class NetworkFeatureConfigRequestBase(components.Component):
    """NetworkFeatureConfigUpdateBase"""
    __polymorphic__ = "NetworkFeatureConfigRequest"


class RouteServerNetworkFeatureConfigBase(components.Component):
    """Routeserver Config Feature"""
    asn = components.IntegerField(
        min_value=0,
        max_value=4294967295,
        help_text="""
            The ASN of the peer.

            Example: 4200000023
        """)
    password = components.CharField(
        required=False,
        default="",
        max_length=128,
        allow_blank=True,
        allow_null=False,
        help_text="""
            The cleartext BGP session password
            Example: bgp-session-test-23
        """)

    as_set_v4 = components.CharField(
        max_length=100,
        required=False,
        allow_null=True,
        help_text="""
            AS-SET of the customer for IPv4 prefix filtering.
            This is used to generate filters on the router servers.

            Only valid referenced prefixes within the AS-SET
            are allowed inbound to the route server. All other routes are
            filtered.

            This field is *required* if the route server network feature only
            supports the `af_inet` address family.
            If multiple address families are supported, it is optional if the
            `as_set_v6` is provided.

            Important: The format has to be: "AS-SET@IRR". IRR is the database
            where the AS-SET is registred. Typically used IRR's are RADB, RIPE,
            NTTCOM, APNIC, ALTDB, LEVEL3, ARIN, AFRINIC, LACNIC

            Example: MOON-AS@RIPE
        """)
    as_set_v6 = components.CharField(
        max_length=100,
        required=False,
        allow_null=True,
        help_text="""
            AS-SET of the customer for IPv6. This is used to generate filters
            on the router servers. Only valid referenced prefixes within
            the AS-SET are allowed inbound to the route server.
            All other routes are filtered.

            This field is *required* if the route server network feature only
            supports the `af_inet6` address family.
            If multiple address families are supported, it is optional if the
            `as_set_v4` is provided.

            Important: The format has to be: "AS-SET@IRR". IRR is the database
            where the AS-SET is registred. Typically used IRR's are RADB, RIPE,
            NTTCOM, APNIC, ALTDB, LEVEL3, ARIN, AFRINIC, LACNIC

            Example: MOON-AS@RIPE
        """)
    max_prefix_v4 = components.IntegerField(
        required=False,
        allow_null=True,
        min_value=0,
        help_text="""
            Announcing more than `max_prefix` IPv4 prefixes the bgp
            session will be droped.

            Example: 5000
        """)
    max_prefix_v6 = components.IntegerField(
        required=False,
        allow_null=True,
        min_value=0,
        help_text="""
            Announcing more than `max_prefix` IPv6 prefixes the bgp
            session will be droped.

            Example: 5000
        """)
    insert_ixp_asn = components.BooleanField(
        default=True,
        required=False,
        help_text="""
            Insert the ASN of the exchange into the AS path. This function is only
            used in special cases. In 99% of all cases, it should be false.

            Example: false
        """)

    session_mode = components.EnumField(
        RouteServerSessionMode,
        help_text="""
            Set the session mode with the routeserver.

            Example: public
        """)
    bgp_session_type = components.EnumField(
        BGPSessionType,
        help_text="""
            The session type describes which of the both parties will open the
            connection. If set to passive, the customer router needs to open
            the connection. If its set to active, the route server will open
            the connection. The standard behavior on most exchanges is passive.

            Example: passive
        """)

    ip = components.PrimaryKeyRelatedField(
        queryset=qs.Any("ipam.IpAddress"),
        help_text="""
            The BGP session will be established from this IP address,
            referenced by ID.

            Only IDs of IPs assigned to the corresponding network service
            config can be used.
        """)


    __polymorphic_type__ = \
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER


class RouteServerNetworkFeatureConfigUpdate(
        NetworkFeatureConfigUpdateBase,
        RouteServerNetworkFeatureConfigBase,
    ):
    """Route Server Network Feature Config Update"""


class RouteServerNetworkFeatureConfigPatch(
        RouteServerNetworkFeatureConfigUpdate,
    ):
    """Route Server Network Feature Config Update"""
    __polymorphic__ = "NetworkFeatureConfigPatch"


class RouteServerNetworkFeatureConfigRequest(
        NetworkFeatureConfigRequestBase,
        NetworkFeatureConfigBase,
        RouteServerNetworkFeatureConfigBase,
    ):
    """Route Server Network Feature Config Request"""


class RouteServerNetworkFeatureConfig(
        events.StatefulBase,
        NetworkFeatureConfigBase,
        RouteServerNetworkFeatureConfigBase,
    ):
    """Route Server Network Feature Config"""
    id = components.CharField(max_length=80)


class NetworkFeatureConfig(components.PolymorphicComponent):
    """Polymorphic Network Feature Config"""
    serializer_classes = {
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
            RouteServerNetworkFeatureConfig,
    }

    entity_types = NETWORK_FEATURE_CONFIG_ENTITIES


class NetworkFeatureConfigRequest(components.PolymorphicComponent):
    """Polymorphic Network Feature Config Request"""
    serializer_classes = {
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
            RouteServerNetworkFeatureConfigRequest,
    }

    entity_types = NETWORK_FEATURE_CONFIG_ENTITIES


class NetworkFeatureConfigUpdate(components.PolymorphicComponent):
    """Polymorphic Network Feauture Config Update"""
    serializer_classes = {
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
            RouteServerNetworkFeatureConfigUpdate,
    }

    entity_types = NETWORK_FEATURE_CONFIG_ENTITIES


class NetworkFeatureConfigPatch(components.PolymorphicComponent):
    """Polymorphic Network Feauture Config Patch"""
    serializer_classes = {
        NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
            RouteServerNetworkFeatureConfigPatch,
    }

    entity_types = NETWORK_FEATURE_CONFIG_ENTITIES
