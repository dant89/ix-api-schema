

"""
Catalog Entities
----------------

This file describes platform entities like devices, facilities,
products, services...

"""

from ixapi_schema.coupling import qs
from ixapi_schema.openapi import components
from ixapi_schema.v2.constants.catalog import (
    PRODUCT_TYPE_CONNECTION,
    PRODUCT_TYPE_EXCHANGE_LAN,
    PRODUCT_TYPE_P2P,
    PRODUCT_TYPE_P2MP,
    PRODUCT_TYPE_MP2MP,
    PRODUCT_TYPE_CLOUD,
    ResourceTypes,
    ProviderVlanTypes,
    DeliveryMethod,
    ServiceProviderWorkflow,
    CrossConnectInitiator,
)


class MetroArea(components.Component):
    """MetroArea"""
    id = components.CharField(max_length=80)

    un_locode = components.CharField(
        max_length=6,
        help_text="""
            The UN/LOCODE for identifying the metro area.

            Example: "DE FRA"
        """)
    iata_code = components.CharField(
        max_length=3,
        help_text="""
            The three letter IATA airport code for identiying the
            metro area.

            Example: "FRA"
        """)

    display_name = components.CharField(
        max_length=64,
        help_text="""
            The name of the metro area. Likely the same as the IATA code.

            Example: "FRA"
        """)

    facilities = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.Facility"),
        many=True,
        help_text="""
            List of facilities the metro area network.
        """)

    metro_area_networks = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.MetroAreaNetwork"),
        many=True,
        help_text="""
            List of networks in the metro area.
        """)


class MetroAreaNetworkBase(components.Component):
    """MetroAreaNetwork"""
    name = components.CharField(
        max_length=32,
        help_text="""
            The name of the metro area network.

            Example: MY-IX-FRA1
        """)
    metro_area = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.MetroArea"),
        help_text="""
            The id of the metro area.

            Example: "met:199399:FRA"
        """)

    service_provider = components.CharField(
        max_length=128,
        help_text="""
            The service provider is operating the network.
            Usually the exchange.

            Example: "MY-IX"
        """)

    pops = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.PointOfPresence"),
        many=True,
        help_text="""
            List of pops in the metro area network.
        """)


class MetroAreaNetwork(MetroAreaNetworkBase):
    """MetroAreaNetwork"""
    id = components.CharField(max_length=80)



class FacilityBase(components.Component):
    """Facility"""
    name = components.CharField(
        max_length=80,
        help_text="""
            Name of the Datacenter as called by the operator

            Example: Crater DC Moon 1
        """)
    metro_area = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.MetroArea"),
        help_text="""
            Id of the `MetroArea` the DC is located in.

            Example: "met:93214980:BER"
        """)
    address_country = components.CharField(
        max_length=2,
        help_text="""
            ISO 3166-1 alpha-2 country code, for example DE

            example: US
        """)
    address_locality = components.CharField(
        max_length=80,
        help_text="""
            The locality/city. For example, Mountain View.
            example: Mountain View
        """)
    address_region = components.CharField(
        max_length=80,
        help_text="""
            The region. For example, CA
            example: CA
        """)
    postal_code = components.CharField(
        max_length=18,
        help_text="""
            A postal code. For example, 9404
            example: "9409"
        """)
    street_address = components.CharField(
        max_length=80,
        help_text="""
            The street address. For example, 1600 Amphitheatre Pkwy.
            example: 1600 Amphitheatre Pkwy.
        """)

    peeringdb_facility_id = components.IntegerField(
        allow_null=True,
        max_value=2147483647,
        min_value=0,
        required=False,
        help_text="""
            [PeeringDB](https://www.peeringdb.com) facitlity ID,
            can be extracted from the url https://www.peeringdb.com/fac/$id

            Example: 103
        """)

    organisation_name = components.CharField(
        max_length=80,
        source="operator_name",
        help_text="""
            Name of Datacenter operator

            Example: Moon Datacenters
        """)

    pops = components.PrimaryKeyRelatedField(
        many=True,
        queryset=qs.Any("catalog.PointOfPresence"),
        help_text="""
            List of pops reachable from the `Facility`.
        """)

    latitude = components.FloatField(
        required=False,
        help_text="""
            Latitude of the facility's location.

            Example: 52.437879
        """)

    longitude = components.FloatField(
        required=False,
        help_text="""
            Longitude of the facility's location.

            Example: 13.650965
        """)



class Facility(FacilityBase):
    """Facility"""
    id = components.CharField(max_length=80)



class DeviceCapability(components.Component):
    """Device Capability"""
    media_type = components.CharField(
        read_only=True,
        max_length=20,
        help_text="""
            The media type of the port (e.g. 1000BASE-LX, 10GBASE-LR, ...)

            Example: "1000BASE-LX"
        """)
    speed = components.IntegerField(
        read_only=True,
        help_text="""
            Speed of port in Mbit/s

            Example: 1000
        """)

    max_lag = components.IntegerField(
        read_only=True,
        max_value=32767,
        min_value=0,
        help_text="""
            Maximum count of ports which can be bundled to a max_lag
            Example: 8
        """)
    availability = components.IntegerField(
        read_only=True,
        source="availability_count",
        max_value=2147483647,
        min_value=0,
        help_text="""
            Count of available ports on device

            Example: 23
        """)


class DeviceBase(components.Component):
    """Device"""
    name = components.CharField(
        max_length=180,
        help_text="""
            Name of the device

            Example: edge2.moon.space-ix.net
        """)

    pop = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.PointOfPresence"),
        help_text="""
            The `PointOfPresence` the device is in.
        """)

    capabilities = DeviceCapability(
        many=True, read_only=True)

    facility = components.PrimaryKeyRelatedField(
        source="physical_facility_id",
        read_only=True,
        help_text="""
            Identifier of the facility where the device
            is physically based.
        """)


class Device(DeviceBase):
    """Device"""
    id = components.CharField(max_length=80)


class DeviceConnectionBase(components.Component):
    """Device Connection"""
    capacity_max = components.IntegerField(
        max_value=2147483647, min_value=0)
    device = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.Device"))
    connected_device = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.Device"))


class DeviceConnection(DeviceConnectionBase):
    """Device Connection"""
    id = components.CharField(max_length=80)


class PointOfPresenceBase(components.Component):
    """Point Of Presence"""
    name = components.CharField(max_length=40)

    facility = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.Facility"),
        help_text="""
            The pop is located in this `Facility`.
        """)

    metro_area_network = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.MetroAreaNetwork"))

    devices = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.Device"),
        many=True)


class PointOfPresence(PointOfPresenceBase):
    """Point Of Presence"""
    id = components.CharField(max_length=80)

    availability_zone = components.PrimaryKeyRelatedField(
        required=False,
        allow_null=True,
        help_text="""
            Availability zone of the pop.
        """,
        queryset=qs.Any("catalog.AvailabilityZone"))


class AvailabilityZoneBase(components.Component):
    """AvailabilityZone"""
    name = components.CharField(
        max_length=80,
        help_text="""
            The name (description) for the availability zone

            Example: "Network A"
        """)


class AvailabilityZone(AvailabilityZoneBase):
    """Availability zone"""
    id = components.CharField(
        max_length=50,
        help_text="""
            The id of the Availability Zone
        """)


class ProductOfferingBase(components.Component):
    """Product Offering Base"""
    id = components.CharField(max_length=80)

    name = components.CharField(
        max_length=160,
        help_text="""
            Name of the product
        """)

    display_name = components.CharField(
        max_length=256,
    )

    exchange_logo = components.CharField(
        required=False,
        help_text="""
            An URI referencing the logo of the internet exchange.

            Example: "https://example.ix/resources/ixpLogo"
        """)

    service_provider_logo = components.CharField(
        required=False,
        help_text="""
            An URI referencing the logo of the service provider.

            Example: "https://example.ix/resources/providerLogo"
        """)

    product_logo = components.CharField(
        required=False,
        help_text="""
            An URI referencing a logo for the product offered.

            Example: "https://example.ix/resources/products/activeCloudPremium"
        """)

    resource_type = components.EnumField(
        ResourceTypes,
        help_text="""
            The resource type refers to an ix-api resource.

            Example: "network_service"
        """)

    handover_metro_area_network = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.MetroAreaNetwork"),
        help_text="""
            Id of the `MetroAreaNetwork`. The service will be accessed
            through the handover metro area network.

            In case of a `p2p_vc`, the `handover_metro_area_network` refers
            to the A-side of the point-to-point connection.
            The A-side is the entity which initiates the network service creation.

            Example: "191239810"
        """)

    handover_metro_area = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.MetroArea"),
        help_text="""
            Id of the `MetroArea`. The network service will be
            accessed from this metro area.

            In case of a `p2p_vc`, the `handover_metro_area` refers
            to the A-side of the point-to-point connection.
            The A-side is the entity which initiates the network service creation.

            Example: "met:29381993:NYC"
        """)


    physical_port_speed = components.IntegerField(
        min_value=0,
        allow_null=True,
        help_text="""
            If the service is dependent on the speed of
            the physical port this field denotes the speed.
        """)

    service_provider = components.CharField(
        allow_null=False,
        help_text="""
            The name of the provider providing the service.

            Example: "AWS"
        """)

    downgrade_allowed = components.BooleanField(
        allow_null=False,
        help_text="""
            Indicates if the service can be migrated to
            a lower bandwidth.
        """)

    upgrade_allowed = components.BooleanField(
        allow_null=False,
        help_text="""
            Indicates if the service can be migrated to
            a higher bandwidth.
        """)

    orderable_not_before = components.DateTimeField(
        required=False,
        allow_null=True,
        help_text="""
            This product offering becomes available for ordering after
            this point in time.
        """)

    orderable_not_after = components.DateTimeField(
        required=False,
        allow_null=True,
        help_text="""
            This product offering will become unavailable for ordering after
            this point in time.
        """)

    contract_terms = components.CharField(
        required=False,
        allow_null=True,
        help_text="""
            The contract terms informally describe the contract period and
            renewals.

            Example: "2 weeks and 1 day, renewing every 37 days afterwards, first 66 days free"
        """)

    notice_period = components.CharField(
        allow_null=True,
        required=False,
        help_text="""
            The notice period informally states constraints
            which define when the client needs to inform the
            IXP in order to prevent renewal of the contract.

            Example: "at least 2 weeks before the end of every odd month"
        """
    )

    __polymorphic__ = "ProductOffering"


class VLanProductOfferingBase(components.Component):
    """A Vlan Product Offering"""
    provider_vlans = components.EnumField(
        ProviderVlanTypes,
        help_text="""
            The `NetworkService` provides `single` or `multi`ple vlans.
        """,
    )


    service_metro_area_network = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.MetroAreaNetwork"),
        help_text="""
            Id of the `MetroAreaNetwork`.
            The service is directly provided on the metro area network.

            In case of a `p2p_vc`, the `service_metro_area_network` refers
            to the B-side of the point-to-point connection.
            The B-side is the accepting party.

            Example: "9123843"
        """)

    service_metro_area = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.MetroArea"),
        help_text="""
            Id of the `MetroArea`. The service is delivered
            in this metro area.

            In case of a `p2p_vc`, the `service_metro_area` refers
            to the B-side of the point-to-point connection.
            The B-side is the accepting party.

            Example: "met:213913485:LON"
        """)

    bandwidth_min = components.IntegerField(
        min_value=0,
        allow_null=True,
        help_text="""
            When configuring access to the network service, at least
            this `capacity` must be provided.
        """)

    bandwidth_max = components.IntegerField(
        min_value=0,
        allow_null=True,
        help_text="""
            When not `null`, this value enforces a mandatory
            rate limit for all network service configs.
        """)

    physical_port_speed = components.IntegerField(
        min_value=0,
        allow_null=True,
        help_text="""
            If the service is dependent on the speed of
            the physical port this field denotes the speed.
        """)


class ConnectionProductOffering(ProductOfferingBase):
    """Connection Product Offering"""
    cross_connect_initiator = components.EnumField(
        CrossConnectInitiator,
        help_text="""
            A cross connect can be initiated by either the
            exchange or the subscriber.

            This property affects which side has to provide
            a LOA and demarc information.
        """)

    handover_pop = components.PrimaryKeyRelatedField(
        queryset=qs.Any("catalog.PointOfPresence"),
        allow_null=True,
        required=False,
        help_text="""
            The ID of the point of presence (see `/pops`), where
            the physical port will be present.

            Example: "pop:127388:LD3"
        """)

    maximum_port_quantity = components.IntegerField(
        min_value=1,
        allow_null=True,
        required=False,
        help_text="""
            The maximum amount of ports which can be aggregated
            in the connection. `null` means no limit.
        """)

    required_contact_roles = components.PrimaryKeyRelatedField(
        source="all_connection_required_contact_roles",
        read_only=True,
        required=False,
        many=True,
        help_text="""
            The connection will require at least one of each of the
            specified roles assigned to contacts.

            The role assignments are associated with the connection
            through the `role_assignments` list property.
        """)

    __polymorphic_type__ = PRODUCT_TYPE_CONNECTION


class ConnectionProductOfferingPatch(
        ConnectionProductOffering,
    ):
    """Conncetion Product Offering"""
    __polymorphic__ = "ProductOfferingPatch"



class ExchangeLanNetworkProductOfferingBase(
        ProductOfferingBase,
        VLanProductOfferingBase):
    """Exchange Lan Network Product Offering"""
    exchange_lan_network_service = components.PrimaryKeyRelatedField(
        queryset=qs.Any("service.NetworkService"),
        help_text="""
            The id of the exchange lan network service.
        """)

    __polymorphic_type__ = PRODUCT_TYPE_EXCHANGE_LAN


class ExchangeLanNetworkProductOffering(ExchangeLanNetworkProductOfferingBase):
    """Exchange Lan Network Product Offering"""


class ExchangeLanNetworkProductOfferingPatch(ExchangeLanNetworkProductOffering):
    """Exchange Lan Network Product Offering"""
    __polymorphic__ = "ProductOfferingPatch"


class P2PNetworkProductOfferingBase(
        ProductOfferingBase,
        VLanProductOfferingBase):
    """P2P Network Product Offering"""
    __polymorphic_type__ = PRODUCT_TYPE_P2P


class P2PNetworkProductOffering(P2PNetworkProductOfferingBase):
    """P2P Network Product Offering"""


class P2PNetworkProductOfferingPatch(
        P2PNetworkProductOffering,
    ):
    """P2P Network Product Offering"""
    __polymorphic__ = "ProductOfferingPatch"


class MP2MPNetworkProductOfferingBase(
        ProductOfferingBase,
        VLanProductOfferingBase):
    """MP2MP Network Product OFfering"""
    __polymorphic_type__ = PRODUCT_TYPE_MP2MP


class MP2MPNetworkProductOffering(MP2MPNetworkProductOfferingBase):
    """MP2MP Network Product Offering"""


class MP2MPNetworkProductOfferingPatch(
        MP2MPNetworkProductOffering,
    ):
    """MP2MP Network Product Offering"""
    __polymorphic__ = "ProductOfferingPatch"


class P2MPNetworkProductOfferingBase(
        ProductOfferingBase,
        VLanProductOfferingBase):
    """P2MP Network Product Offering"""
    __polymorphic_type__ = PRODUCT_TYPE_P2MP


class P2MPNetworkProductOffering(P2MPNetworkProductOfferingBase):
    """P2MP Network Product Offering"""


class P2MPNetworkProductOfferingPatch(
        P2MPNetworkProductOffering,
    ):
    """P2MP Network Product Offering"""
    __polymorphic__ = "ProductOfferingPatch"


class CloudNetworkProductOfferingBase(
        ProductOfferingBase,
        VLanProductOfferingBase):
    """Cloud Network Product Offering"""
    service_provider_region = components.CharField(
        allow_null=True,
        help_text="""
            The service provider offers the network service for a
            specific region.

            Example: "eu-central-1"
        """)

    service_provider_pop = components.CharField(
        allow_null=True,
        help_text="""
            The datacenter id of the partner NNI to the service provider.
            It supposed to be used when identifying a location via
            the cloud provider's APIs.

            Example: "INX6"
        """)

    service_provider_pop_name = components.CharField(
        allow_null=True,
        required=False,
        help_text="""
            The datacenter description of the partner NNI to the service provider.

            Example: "Interxion FRA6, Frankfurt, DEU"
        """)

    service_provider_workflow = components.EnumField(
        ServiceProviderWorkflow,
        help_text="""
            When the workflow is `provider_first` the subscriber creates
            a circuit with the cloud provider and provides a `cloud_key` for filtering
            the product-offerings.

            If the workflow is `exchange_first` the IX will create
            the cloud circuit on the provider side.

            Example: "exchange_first"
        """)

    delivery_method = components.EnumField(
        DeliveryMethod,
        help_text="""
            The exchange delivers the service over a `shared` or `dedicated` NNI.
        """)

    diversity = components.IntegerField(
        min_value=1,
        allow_null=False,
        help_text="""
            The service can be delivered over multiple handovers from
            the exchange to the `service_provider`.
            The `diversity` denotes the number of handovers between the 
            exchange and the service provider. A value of two signals a
            redundant service.

            Only one network service configuration for each `handover` and
            `cloud_vlan` can be created.
        """)


    __polymorphic_type__ = PRODUCT_TYPE_CLOUD


class CloudNetworkProductOffering(CloudNetworkProductOfferingBase):
    """Cloud Network Product Offering"""


class CloudNetworkProductOfferingPatch(
        CloudNetworkProductOffering,
    ):
    """Cloud Network Product Offering"""
    __polymorphic__ = "ProductOfferingPatch"


class ProductOffering(components.PolymorphicComponent):
    """Polymorphic Product Offering"""
    serializer_classes = {
        PRODUCT_TYPE_CONNECTION:
            ConnectionProductOffering,
        PRODUCT_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkProductOffering,
        PRODUCT_TYPE_P2P:
            P2PNetworkProductOffering,
        PRODUCT_TYPE_MP2MP:
            MP2MPNetworkProductOffering,
        PRODUCT_TYPE_P2MP:
            P2MPNetworkProductOffering,
        PRODUCT_TYPE_CLOUD:
            CloudNetworkProductOffering,
    }

    entity_types = {
        "catalog.ConnectionProductOffering":
            PRODUCT_TYPE_CONNECTION,
        "catalog.ExchangeLanNetworkProductOffering":
            PRODUCT_TYPE_EXCHANGE_LAN,
        "catalog.P2PNetworkProductOffering":
            PRODUCT_TYPE_P2P,
        "catalog.MP2MPNetworkProductOffering":
            PRODUCT_TYPE_MP2MP,
        "catalog.P2MPNetworkProductOffering":
            PRODUCT_TYPE_P2MP,
        "catalog.CloudNetworkProductOffering":
            PRODUCT_TYPE_CLOUD,
    }


class ProductOfferingPatch(components.PolymorphicComponent):
    """Polymorphic Product Offering"""
    serializer_classes = {
        PRODUCT_TYPE_CONNECTION:
            ConnectionProductOfferingPatch,
        PRODUCT_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkProductOfferingPatch,
        PRODUCT_TYPE_P2P:
            P2PNetworkProductOfferingPatch,
        PRODUCT_TYPE_MP2MP:
            MP2MPNetworkProductOfferingPatch,
        PRODUCT_TYPE_P2MP:
            P2MPNetworkProductOfferingPatch,
        PRODUCT_TYPE_CLOUD:
            CloudNetworkProductOfferingPatch,
    }

    entity_types = {
        "catalog.ConnectionProductOfferingPatch":
            PRODUCT_TYPE_CONNECTION,
        "catalog.ExchangeLanNetworkProductOfferingPatch":
            PRODUCT_TYPE_EXCHANGE_LAN,
        "catalog.P2PNetworkProductOfferingPatch":
            PRODUCT_TYPE_P2P,
        "catalog.MP2MPNetworkProductOfferingPatch":
            PRODUCT_TYPE_MP2MP,
        "catalog.P2MPNetworkProductOfferingPatch":
            PRODUCT_TYPE_P2MP,
        "catalog.CloudNetworkProductOfferingPatch":
            PRODUCT_TYPE_CLOUD,
    }

