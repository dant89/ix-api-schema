

from ixapi_schema.openapi import components


DESCRIPTION_STATS_AGGREGATED = """
    A `start` and `end` query parameter can be used to
    retrieve the aggregated traffic for a given window.
    In this case the key of the returned statistics is `custom`.

    With a given `start` and `end` window, the resolution for
    the aggregated data is chosen by the implementation.

    You need to check the `accuracy` attribute of the aggregate,
    to see if the data can be used for the desired
    usecase. The `accuracy` is the ratio of *total samples* to
    *expected samples*.

    If no `start` or `end` parameter is given, a sliding window
    is assumed and key value pairs of resolutions and aggregated
    statistics are returned.
"""


class AggregateStatistics(components.Component):
    """Statistics"""
    title = components.CharField(
        required=True,
        help_text="""
            Title of the aggregated statistics.

            Example: "30 Days"
        """)

    start = components.DateTimeField(
        required=True,
        help_text="""
            Start of the traffic aggregation.
        """)

    end = components.DateTimeField(
        required=True,
        help_text="""
            End of the traffic aggregation.
        """)

    accuracy = components.FloatField(
        required=True,
        min_value=0.0,
        max_value=1.0,
        help_text="""
            The accuracy is the ratio of *total aggregated samples* to
            *expected samples*.

            The expected number of samples is the size of the window
            of the aggregate, divided by the aggregation resolution.

            For example: A window of `24 h` with an aggregation resolution
            of `5 m` would yield `288` samples.

            If only `275` samples are available for aggregation, the
            accuracy would be `0.95`.

            Example: 0.95
        """)

    created_at = components.DateTimeField(
        required=True,
        help_text="""
            Timestamp when the statistics were created.
        """)

    next_update_at = components.DateTimeField(
        required=True,
        help_text="""
            Next update of the statistical data.
            This may not correspond to the aggregate interval.
        """)

    # Average
    average_pps_in = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            Average number of inbound **packets per second**.

            Example: 1730224
        """)

    average_pps_out = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            Average number outbound **packets per second**.

            Example: 17456
        """)

    average_ops_in = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            Average inbound **octets per second**.

            Example: 1734882240
        """)

    average_ops_out = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            Average outbound **octets per second**.

            Example: 173220
        """)


    average_eps_in = components.FloatField(
        required=False,
        min_value=0,
        help_text="""
            Average **errors per second** inbound.

            Example: 0.32
        """)

    average_eps_out = components.FloatField(
        required=False,
        min_value=0,
        help_text="""
            Averages **errors per second** outbound.

            Example: 0.01
        """)

    average_dps_in = components.FloatField(
        required=False,
        min_value=0,
        help_text="""
            Average **discards per second** inbound.

            Example: 0.01
        """)

    average_dps_out = components.FloatField(
        required=False,
        min_value=0,
        help_text="""
            Averages **discards per second** outbound.

            Example: 0.12
        """)

    # 95th percentile
    percentile95_pps_in = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            95th percentile of inbound **packets per second**.

            Example: 1730224
        """)

    percentile95_pps_out = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            95th percentile of outbound **packets per second**.

            Example: 17456
        """)

    percentile95_pps_in = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            95th percentile of the inbound **octets per second**.

            Example: 1734882240
        """)

    percentile95_ops_out = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            95th percentile of outbound **octets per second**.

            Example: 173220
        """)

    # Peak
    maximum_pps_in = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            Peak inbound **packets per second** during the interval.

            Example: 1730224
        """)

    maximum_pps_out = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            Peak outbound **packets per second** during the interval.

            Example: 17456
        """)

    maximum_ops_in = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            Peak inbound **octets per second** during the interval.

            Example: 5734882240
        """)

    maximum_ops_out = components.IntegerField(
        required=False,
        min_value=0,
        help_text="""
            Peak outbound **octets per second** during the interval.

            Example: 473220
        """)

    maximum_in_at = components.DateTimeField(
        required=False,
        help_text="""
            Timestamp when the inbound peak occured.
        """)

    maximum_out_at = components.DateTimeField(
        required=False,
        help_text="""
            Timestamp when the outbound peak occured.
        """)


class Aggregate(
        components.Component,
    ):
    """
    Mapping of Aggregated Statistics
    """
    aggregates = components.DictField(
        child=AggregateStatistics(),
        required=True,
        help_text="""
            Aggregated statistics for a connection or service configuration

            For the **property name** the aggregate interval as a
            string representation is used. For example: `5m`, `1d`, `30d`,
            `1y`.

            If a window is defined via the `start` and `end` query parameter,
            the **property name** will be `custom`.

            The available intervals can differ by implementation.

            Example: ###AGGREGATE_EXAMPLE###
        """)


class PortStatistics(Aggregate, components.Component):
    """Port Statistics"""
    light_levels_tx = components.ListField(
        child=components.FloatField(),
        help_text="""
            A list of light levels in **dBm** for each channel.

            Example: [-5.92, -5.29]
        """)
    light_levels_rx = components.ListField(
        child=components.FloatField(),
        help_text="""
            A list of light levels in **dBm** for each channel.

            Example: [-2.92, -3.89]
        """)





class AggregateTimeseries(
        components.Component,
    ):
    """
    Aggregated Statistics Timeseries
    """
    title = components.CharField(
        required=True,
        help_text="""
            Title of the timeseries.

            Example: "5 Minutes"
        """)

    precision = components.IntegerField(
        required=True,
        min_value=0,
        help_text="""
            Precision indicates the sampling rate of the aggregated traffic data
            in seconds.
            For example if the data is aggregated over 5 minutes, the precision
            would be 300.

            Example: 300
        """)


    created_at = components.DateTimeField(
        required=True,
        help_text="""
            Timestamp when the statistics were created.
        """)

    next_update_at = components.DateTimeField(
        required=True,
        help_text="""
            Next update of the statistical data.
            This may not correspond to the aggregate interval.
        """)

    origin_timezone = components.CharField(
        required=True,
        help_text="""
            The timezone where the data was collected in tz database
            format.

            Example: "Europe/Amsterdam"
        """)

    samples = components.ListField(
        required=True,
        child=components.TimeseriesSampleField(),
        help_text="""
            A list of timeseries samples, where samples are n-tuples, where
            the first element is a timestamp.

            * `timestamp` (DateTime, `string`, ISO 8601, Example: `"2001-10-23T23:42:10Z"`)

            The other fields can be selected in the `fields` query from the
            aggregates. Default fields are:

            * `average_pps_in`  (`integer`)
            * `average_pps_out` (`integer`)
            * `average_ops_in` (`integer`)
            * `average_ops_out` (`integer`)

            Missing values in the timeseries are represented by `null`.
        """)
